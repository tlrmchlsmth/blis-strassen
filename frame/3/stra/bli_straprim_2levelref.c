
#include "blis.h"

void bli_mulstraprim_ref( dim_t   len,
                          obj_t**  alpha_list, 
                          obj_t**  a_list, obj_t**  gamma_list,
                          obj_t**  b_list, obj_t**  delta_list,
                          obj_t**  beta_list, 
                          obj_t**  c_list)
{

	num_t dt = bli_obj_datatype( **c_list );
    dim_t m  = bli_obj_length( **c_list );
    dim_t n  = bli_obj_width( **c_list );
    dim_t k  = bli_obj_width( **a_list );

    obj_t a, b, c_tmp;
    bli_obj_create( dt, m, k, 0, 0, &a );
    bli_obj_create( dt, k, n, 0, 0, &b );
    bli_obj_create( dt, m, n, 0, 0, &c_tmp );

    bli_copym( a_list[ 0 ], &a );

    for ( dim_t i = 1; i < len; i++ ) {
        if( bli_obj_buffer( *a_list[ i ] ) != NULL ) {
            bli_axpym( gamma_list[ i ], a_list[i], &a );
        }
    }

    bli_copym( b_list[ 0 ], &b );

    for ( dim_t i = 1; i < len; i++ ) {
        if( bli_obj_buffer( *b_list[ i ] ) != NULL ) {
            bli_axpym( delta_list[ i ], b_list[i], &b );
        }
    }

    bli_gemm( &BLIS_ONE, &a, &b, &BLIS_ZERO, &c_tmp );

    bli_axpym( alpha_list[0], &c_tmp, c_list[0] );

    for ( dim_t i = 1; i < len; i++ ) {
        if( bli_obj_buffer( *c_list[ i ] ) != NULL ) {
            bli_axpym( alpha_list[ i ], &c_tmp, c_list[ i ] );
        }
    }

    bli_obj_free( &a );
    bli_obj_free( &b );
    bli_obj_free( &c_tmp );
}
