
void mkl_copym( obj_t *a, obj_t *b );

void mkl_axpym( obj_t *alpha, obj_t *a, obj_t *b );

void mkl_gemm( obj_t *alpha, obj_t *a, obj_t *b, obj_t *beta, obj_t *c );

void mkl_scalm( obj_t *alpha, obj_t *a );

void bli_straprim_mkl_ref( obj_t*  alpha1, obj_t*  alpha2,
                           obj_t*  aA, obj_t*  gamma, obj_t*  aB,
                           obj_t*  bA, obj_t*  delta, obj_t*  bB,
                           obj_t*  beta1, obj_t*  beta2,
                           obj_t*  cA, obj_t*  cB );

void bli_mulstraprim_mkl_ref( dim_t   len,
                              obj_t**  alpha_list, 
                              obj_t**  a_list, obj_t**  gamma_list,
                              obj_t**  b_list, obj_t**  delta_list,
                              obj_t**  beta_list, 
                              obj_t**  c_list);

void bli_straprim_hybrid( obj_t*  alpha1, obj_t*  alpha2,
                          obj_t*  aA, obj_t*  gamma, obj_t*  aB,
                          obj_t*  bA, obj_t*  delta, obj_t*  bB,
                          obj_t*  beta1, obj_t*  beta2,
                          obj_t*  cA, obj_t*  cB );

