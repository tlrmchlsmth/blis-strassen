
#include "blis.h"

#include "assert.h"

void bli_straprim_rec_ref( dim_t   len,
                       obj_t**  alpha_list, 
                       obj_t**  a_list, obj_t**  gamma_list,
                       obj_t**  b_list, obj_t**  delta_list,
                       obj_t**  beta_list, 
                       obj_t**  c_list)
{
    dim_t m = bli_obj_length( **c_list );
    dim_t n = bli_obj_width( **c_list );
    dim_t k = bli_obj_width( **a_list );
    assert( m % 2 == 0);
    assert( n % 2 == 0);
    assert( k % 2 == 0);

    if ( len == STRA_LIST_LEN ) {

        //#include "blis_straprim_multilevel_print.h"

        bli_mulstraprim_mkl_ref( len,
                             alpha_list, 
                             a_list, gamma_list,
                             b_list, delta_list,
                             beta_list, 
                             c_list);
        return;
    }

    dim_t next_len = len * 2;
    obj_t **a_next_list, **b_next_list, **c_next_list;
    obj_t **alpha_next_list, **gamma_next_list, **delta_next_list, **beta_next_list;
    a_next_list = (obj_t **)malloc(next_len*sizeof(obj_t *));
    b_next_list = (obj_t **)malloc(next_len*sizeof(obj_t *));
    c_next_list = (obj_t **)malloc(next_len*sizeof(obj_t *));
    alpha_next_list = (obj_t **)malloc(next_len*sizeof(obj_t *));
    gamma_next_list = (obj_t **)malloc(next_len*sizeof(obj_t *));
    delta_next_list = (obj_t **)malloc(next_len*sizeof(obj_t *));
    beta_next_list = (obj_t **)malloc(next_len*sizeof(obj_t *));
    
    //Should define a global variable
    obj_t a_null, b_null, c_null, scalar_null;
    bli_obj_create_without_buffer( bli_obj_datatype( **a_list ), bli_obj_length( **a_list )/2, bli_obj_width( **a_list )/2, &a_null );
    bli_obj_create_without_buffer( bli_obj_datatype( **b_list ), bli_obj_length( **b_list )/2, bli_obj_width( **b_list )/2, &b_null );
    bli_obj_create_without_buffer( bli_obj_datatype( **c_list ), bli_obj_length( **c_list )/2, bli_obj_width( **c_list )/2, &c_null );
    //bli_obj_create_without_buffer( bli_obj_datatype( c00 ), 1, 1, scalar_null );
    scalar_null = BLIS_ZERO;

    //len    alpha_next_list += alpha_list_i (*) alpha1_cur(1),alpha2_cur(1)
    //len    a_next_list += a_list_i_0, a_list_i_3;
    //len-1  gamma_next_list += gamma_list_i (*) 1, gamma_cur(1)
    //len    b_next_list += b_list_i_0, b_list_i_3;
    //len-1  delta_next_list += delta_list_i (*) 1, delta_cur(1)
    //len    c_next_list += c_list_i_0, c_list_i_3;
    // M1
    // c00 = 0*c00+1*(a00+a11)(b00+b11)
    // c11 = 0*c11+1*(a00+a11)(b00+b11)
    //    //CONST_OBJ Multiplication???
    STRAPRIM_WRAPPER( 1.0, 1.0, \
                      1.0, 1.0, \
                      1.0, 1.0, \
                      0, 3, \
                      0, 3, \
                      0, 3, \
                      bli_straprim_rec_ref \
                    );

    // M2
    // c10 = 1*c10+1*(a10+a11)b00
    // c11 = 1*c11-1*(a10+a11)b00
    STRAPRIM_WRAPPER( 1.0, -1.0, \
                      1.0, 1.0, \
                      1.0, 0.0, \
                      2, 3, \
                      0, -1, \
                      2, 3, \
                      bli_straprim_rec_ref \
                    );

    // M3
    // c01 = 1*c01+1*a00(b01-b11)
    // c11 = 1*c11+1*a00(b01-b11)
    STRAPRIM_WRAPPER( 1.0, 1.0, \
                      1.0, 0.0, \
                      1.0, -1.0, \
                      0, -1, \
                      1, 3, \
                      1, 3, \
                      bli_straprim_rec_ref \
                    );

    // M4
    // c00 = 1*c00+1*a11(b10-b00)
    // c10 = 1*c10+1*a11(b10-b00)
    STRAPRIM_WRAPPER( 1.0, 1.0, \
                      1.0, 0.0, \
                      1.0, -1.0, \
                      3, -1, \
                      2, 0, \
                      0, 2, \
                      bli_straprim_rec_ref \
                    );

    // M5
    // c00 = 1*c00-1*(a00+a01)b11
    // c01 = 1*c01+1*(a00+a01)b11
    STRAPRIM_WRAPPER( -1.0, 1.0, \
                      1.0, 1.0, \
                      1.0, 0.0, \
                      0, 1, \
                      3, -1, \
                      0, 1, \
                      bli_straprim_rec_ref \
                    );

    // M6
    // c11 = 1*c11+(a10-a00)(b00+b01)
    STRAPRIM_WRAPPER( 1.0, 0.0, \
                      1.0, -1.0, \
                      1.0, 1.0, \
                      2, 0, \
                      0, 1, \
                      3, -1, \
                      bli_straprim_rec_ref \
                    );//if (c_null->is_dummy==1) do nothing for cB

    // M7
    // c00 = 1*c00+(a01-a11)(b10+b11)
    STRAPRIM_WRAPPER( 1.0, 0.0, \
                      1.0, -1.0, \
                      1.0, 1.0, \
                      1, 3, \
                      2, 3, \
                      0, -1, \
                      bli_straprim_rec_ref \
                    );//if (c_null->is_dummy==1) do nothing for cB


    bli_obj_free( &a_null );
    bli_obj_free( &b_null );
    bli_obj_free( &c_null );

    //free( alpha, beta, gamma, delta )
    // for loop...if != a_null/b_null/c_null -> free...
}

void bli_stra_2levelref( obj_t* a, obj_t* b, obj_t* c )
{
    dim_t m = bli_obj_length( *c );
    dim_t n = bli_obj_width( *c );
    dim_t k = bli_obj_width( *a );

    //Pretty sure we can handle the following cases,
    //but for now, let's make sure we're working with square even dimensioned matrices.
    assert( m % 2 == 0);
    assert( n % 2 == 0);
    assert( k % 2 == 0);
    //assert( m == n);
    //assert( m == k);

    //Partition
    obj_t a00, a01;
    obj_t a10, a11;
    bli_acquire_mpart_half( BLIS_SUBPART00, a, &a00);
    bli_acquire_mpart_half( BLIS_SUBPART01, a, &a01);
    bli_acquire_mpart_half( BLIS_SUBPART10, a, &a10);
    bli_acquire_mpart_half( BLIS_SUBPART11, a, &a11);

    obj_t b00, b01;
    obj_t b10, b11;
    bli_acquire_mpart_half( BLIS_SUBPART00, b, &b00);
    bli_acquire_mpart_half( BLIS_SUBPART01, b, &b01);
    bli_acquire_mpart_half( BLIS_SUBPART10, b, &b10);
    bli_acquire_mpart_half( BLIS_SUBPART11, b, &b11);

    obj_t c00, c01;
    obj_t c10, c11;
    bli_acquire_mpart_half( BLIS_SUBPART00, c, &c00);
    bli_acquire_mpart_half( BLIS_SUBPART01, c, &c01);
    bli_acquire_mpart_half( BLIS_SUBPART10, c, &c10);
    bli_acquire_mpart_half( BLIS_SUBPART11, c, &c11);

    obj_t a_null, b_null, c_null, scalar_null;
    bli_obj_create_without_buffer( bli_obj_datatype( a00 ), bli_obj_length( a00 ), bli_obj_width( a00 ), &a_null );
    bli_obj_create_without_buffer( bli_obj_datatype( b00 ), bli_obj_length( b00 ), bli_obj_width( b00 ), &b_null );
    bli_obj_create_without_buffer( bli_obj_datatype( c00 ), bli_obj_length( c00 ), bli_obj_width( c00 ), &c_null );
    //bli_obj_create_without_buffer( bli_obj_datatype( c00 ), 1, 1, scalar_null );
    scalar_null = BLIS_ZERO;

    int   len = 2;
    obj_t **a_list, **b_list, **c_list;
    obj_t **alpha_list, **gamma_list, **delta_list, **beta_list;
    a_list = (obj_t **)malloc(len*sizeof(obj_t *));
    b_list = (obj_t **)malloc(len*sizeof(obj_t *));
    c_list = (obj_t **)malloc(len*sizeof(obj_t *));
    alpha_list = (obj_t **)malloc(len*sizeof(obj_t *));
    gamma_list = (obj_t **)malloc(len*sizeof(obj_t *));
    delta_list = (obj_t **)malloc(len*sizeof(obj_t *));
    beta_list = (obj_t **)malloc(len*sizeof(obj_t *));
 

    ////num_t dt = bli_obj_datatype( c00 );
    //num_t dt = BLIS_DOUBLE;
    //obj_t c00_ref, c11_ref;
    //bli_obj_create( dt, m/2, n/2, 0, 0, &c00_ref );
    //bli_obj_create( dt, m/2, n/2, 0, 0, &c11_ref );
    //bli_copym( &c00, &c00_ref );
    //bli_copym( &c11, &c11_ref );


    //Now do the 7 straprim thingers

    // M1
    // c00 = 0*c00+1*(a00+a11)(b00+b11)
    // c11 = 0*c11+1*(a00+a11)(b00+b11)
    //bli_straprim_rec( 2,
    //              &BLIS_ONE, &BLIS_ONE,
    //              &a00, &BLIS_ONE, &a11, 
    //              &b00, &BLIS_ONE, &b11, 
    //              &BLIS_ONE, &BLIS_ONE,
    //              &c00, &c11 );
    //bli_straprim( &BLIS_ONE, &BLIS_ONE,
    //              &a00, &BLIS_ONE, &a11, 
    //              &b00, &BLIS_ONE, &b11, 
    //              &BLIS_ZERO, &BLIS_ZERO,
    //              &c00, &c11 );
    STRAPRIM_WRAPPER_2( BLIS_ONE, BLIS_ONE,
                        BLIS_ONE, BLIS_ONE,
                        BLIS_ONE, BLIS_ONE,
                        BLIS_ONE, BLIS_ONE,
                        a00, a11,
                        b00, b11,
                        c00, c11,
                        bli_straprim_rec_ref
                        );


    // M2
    // c10 = 0*c10+1*(a10+a11)b00
    // c11 = 1*c11-1*(a10+a11)b00
    //bli_straprim( &BLIS_ONE, &BLIS_MINUS_ONE,
    //              &a10, &BLIS_ONE, &a11, 
    //              &b00, &scalar_null, &b_null,
    //              &BLIS_ZERO, &BLIS_ONE,
    //              &c10, &c11 );
    //bli_straprim( &BLIS_ONE, &BLIS_MINUS_ONE,
    //              &a10, &BLIS_ONE, &a11, 
    //              &b00, &scalar_null, &b_null,
    //              &BLIS_ONE, &BLIS_ONE,
    //              &c10, &c11 );
    STRAPRIM_WRAPPER_2( BLIS_ONE, BLIS_MINUS_ONE,
                        BLIS_ONE, BLIS_ONE,
                        BLIS_ONE, scalar_null,
                        BLIS_ONE, BLIS_ONE,
                        a10, a11,
                        b00, b_null,
                        c10, c11,
                        bli_straprim_rec_ref
                        );


    // M3
    // c01 = 0*c01+1*a00(b01-b11)
    // c11 = 1*c11+1*a00(b01-b11)
    //bli_straprim( &BLIS_ONE, &BLIS_ONE,
    //              &a00, &scalar_null, &a_null, 
    //              &b01, &BLIS_MINUS_ONE, &b11,
    //              &BLIS_ZERO, &BLIS_ONE,
    //              &c01, &c11 );
    //bli_straprim( &BLIS_ONE, &BLIS_ONE,
    //              &a00, &scalar_null, &a_null, 
    //              &b01, &BLIS_MINUS_ONE, &b11,
    //              &BLIS_ONE, &BLIS_ONE,
    //              &c01, &c11 );
    STRAPRIM_WRAPPER_2( BLIS_ONE, BLIS_ONE,
                        BLIS_ONE, scalar_null,
                        BLIS_ONE, BLIS_MINUS_ONE,
                        BLIS_ONE, BLIS_ONE,
                        a00, a_null,
                        b01, b11,
                        c01, c11,
                        bli_straprim_rec_ref
                        );


    // M4
    // c00 = 1*c00+1*a11(b10-b00)
    // c10 = 1*c10+1*a11(b10-b00)
    //bli_straprim( &BLIS_ONE, &BLIS_ONE,
    //              &a11, &scalar_null, &a_null, 
    //              &b10, &BLIS_MINUS_ONE, &b00,
    //              &BLIS_ONE, &BLIS_ONE,
    //              &c00, &c10 );
    STRAPRIM_WRAPPER_2( BLIS_ONE, BLIS_ONE,
                        BLIS_ONE, scalar_null,
                        BLIS_ONE, BLIS_MINUS_ONE,
                        BLIS_ONE, BLIS_ONE,
                        a11, a_null,
                        b10, b00,
                        c00, c10,
                        bli_straprim_rec_ref
                        );




    // M5
    // c00 = 1*c00-1*(a00+a01)b11
    // c01 = 1*c01+1*(a00+a01)b11
    //bli_straprim( &BLIS_MINUS_ONE, &BLIS_ONE,
    //              &a00, &BLIS_ONE, &a01, 
    //              &b11, &scalar_null, &b_null,
    //              &BLIS_ONE, &BLIS_ONE,
    //              &c00, &c01 );
    STRAPRIM_WRAPPER_2( BLIS_MINUS_ONE, BLIS_ONE,
                        BLIS_ONE, BLIS_ONE,
                        BLIS_ONE, scalar_null,
                        BLIS_ONE, BLIS_ONE,
                        a00, a01,
                        b11, b_null,
                        c00, c01,
                        bli_straprim_rec_ref
                        );




    // M6
    // c11 = 1*c11+(a10-a00)(b00+b01)
    //bli_straprim( &BLIS_ONE, &scalar_null,
    //              &a10, &BLIS_MINUS_ONE, &a00, 
    //              &b00, &BLIS_ONE, &b01,
    //              &BLIS_ONE, &scalar_null,
    //              &c11, &c_null );                     //if (c_null->is_dummy==1) do nothing for cB
    STRAPRIM_WRAPPER_2( BLIS_ONE, scalar_null,
                        BLIS_ONE, BLIS_MINUS_ONE,
                        BLIS_ONE, BLIS_ONE,
                        BLIS_ONE, BLIS_ONE,
                        a10, a00,
                        b00, b01,
                        c11, c_null,
                        bli_straprim_rec_ref
                        );




    // M7
    // c00 = 1*c00+(a01-a11)(b10+b11)
    //bli_straprim( &BLIS_ONE, &scalar_null,
    //              &a01, &BLIS_MINUS_ONE, &a11, 
    //              &b10, &BLIS_ONE, &b11,
    //              &BLIS_ONE, &scalar_null,
    //              &c00, &c_null );
    STRAPRIM_WRAPPER_2( BLIS_ONE, scalar_null,
                        BLIS_ONE, BLIS_MINUS_ONE,
                        BLIS_ONE, BLIS_ONE,
                        BLIS_ONE, BLIS_ONE,
                        a01, a11,
                        b10, b11,
                        c00, c_null,
                        bli_straprim_rec_ref
                        );


    bli_obj_free( &a_null );
    bli_obj_free( &b_null );
    bli_obj_free( &c_null );
}
