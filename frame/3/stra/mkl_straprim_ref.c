
#include "blis.h"

void mkl_copym( obj_t *a, obj_t *b ) {

    dim_t m = bli_obj_length( *a );
    dim_t n = bli_obj_width( *a );

    inc_t lda = bli_obj_col_stride( *a ); 
    inc_t ldb = bli_obj_col_stride( *b ); 

    double *buf_a = bli_obj_buffer_at_off( *a );
    double *buf_b = bli_obj_buffer_at_off( *b );

    inc_t incx = 1;
    inc_t incy = 1;



    ////#pragma omp parallel for private( cur_buf_a, cur_buf_b ) schedule( dynamic )
    #pragma omp parallel for schedule( dynamic )
    for ( dim_t ii = 0; ii < n; ii ++ ) {
        double *cur_buf_a, *cur_buf_b;
        cur_buf_a = &buf_a[ ii * lda ];
        cur_buf_b = &buf_b[ ii * ldb ];
        dcopy_( &m, cur_buf_a, &incx, cur_buf_b, &incy);
    }

}

// b = b + alpha * a
void mkl_axpym( obj_t *alpha, obj_t *a, obj_t *b ) {


    dim_t m = bli_obj_length( *a );
    dim_t n = bli_obj_width( *a );

    inc_t lda = bli_obj_col_stride( *a ); 
    inc_t ldb = bli_obj_col_stride( *b ); 

    //printf( "m: %lu, n: %lu, lda: %lu, ldb: %lu\n", m, n, lda, ldb );

    double *buf_a = bli_obj_buffer_at_off( *a );
    double *buf_b = bli_obj_buffer_at_off( *b );

    double* buf_alpha  = bli_obj_buffer_for_const(BLIS_DOUBLE, *alpha );

    //printf( "before:buf_a[0]: %lf, buf_b[0]: %lf, *buf_alpha: %lf\n", buf_a[0], buf_b[0], *buf_alpha );


    inc_t incx = 1;
    inc_t incy = 1;

    //dim_t k = m * n;

    ////#pragma omp parallel for private( cur_buf_a, cur_buf_b ) schedule( dynamic )
    #pragma omp parallel for schedule( dynamic )
    for ( dim_t ii = 0; ii < n; ii ++ ) {
        double *cur_buf_a, *cur_buf_b;
        cur_buf_a = &buf_a[ ii * lda ];
        cur_buf_b = &buf_b[ ii * ldb ];
        daxpy_( &m, buf_alpha, cur_buf_a, &incx, cur_buf_b, &incy );
        //buf_a += lda;
        //buf_b += ldb;
    }

    //printf( "after:buf_a[0]: %lf, buf_b[0]: %lf, *buf_alpha: %lf\n", buf_a[0], buf_b[0], *buf_alpha );


}

// c = beta * c + alpha * a * b 
void mkl_gemm( obj_t *alpha, obj_t *a, obj_t *b, obj_t *beta, obj_t *c ) {
    char ta = 'N';
    char tb = 'N';

    dim_t m = bli_obj_length( *c );
    dim_t n = bli_obj_width( *c );
    //dim_t k = bli_obj_width( *a );
    dim_t k = bli_obj_width_after_trans( *a );

    double* buf_alpha  = bli_obj_buffer_for_const(BLIS_DOUBLE, *alpha );
    double* buf_beta   = bli_obj_buffer_for_const(BLIS_DOUBLE, *beta  );

    double *buf_a = bli_obj_buffer_at_off( *a );
    double *buf_b = bli_obj_buffer_at_off( *b );
    double *buf_c = bli_obj_buffer_at_off( *c );

    //inc_t rs_x = bli_obj_row_stride( a1A ); 
    inc_t lda = bli_obj_col_stride( *a ); 
    inc_t ldb = bli_obj_col_stride( *b ); 
    inc_t ldc = bli_obj_col_stride( *c ); 

    dgemm_(&ta, &tb, &m, &n, &k, buf_alpha, buf_a, &lda, buf_b, &ldb, buf_beta, buf_c, &ldc);

}

void mkl_scalm( obj_t *alpha, obj_t *a ) {

    dim_t m = bli_obj_length( *a );
    dim_t n = bli_obj_width( *a );

    inc_t lda = bli_obj_col_stride( *a ); 

    double *buf_a = bli_obj_buffer_at_off( *a );

    double* buf_alpha  = bli_obj_buffer_for_const( BLIS_DOUBLE, *alpha );


    inc_t incx = 1;

    ////#pragma omp parallel for private( cur_buf_a ) schedule( dynamic )
    #pragma omp parallel for schedule( dynamic )
    for ( dim_t ii = 0; ii < n; ii ++ ) {
        double *cur_buf_a;
        cur_buf_a = &buf_a[ ii * lda ];
        dscal_( &m, buf_alpha, cur_buf_a, &incx);
    }

}



void bli_straprim_mkl_ref( obj_t*  alpha1, obj_t*  alpha2,
                           obj_t*  aA, obj_t*  gamma, obj_t*  aB,
                           obj_t*  bA, obj_t*  delta, obj_t*  bB,
                           obj_t*  beta1, obj_t*  beta2,
                           obj_t*  cA, obj_t*  cB ) {

	num_t dt = bli_obj_datatype( *cA );
    dim_t m  = bli_obj_length( *cA );
    dim_t n  = bli_obj_width( *cA );
    dim_t k  = bli_obj_width( *aA );

    obj_t a, b, c_tmp;
    bli_obj_create( dt, m, k, 0, 0, &a );
    bli_obj_create( dt, k, n, 0, 0, &b );
    bli_obj_create( dt, m, n, 0, 0, &c_tmp );

    //dtime = bli_clock();
    //bli_straprim_ref( &alpha1, &alpha2,
    //                  &aA, &gamma, aB, 
    //                  &bA, &delta, &bB, 
    //                  &beta1, &beta2,
    //                  &cA_strassen, &cB_strassen );

    //cA = beta1*cA+alpha1*(aA+gamma*aB)(bA+delta*bB)
    //cB = beta2*cB+alpha2*(aA+gamma*aB)(bA+delta*bB) 
    //a:=aA
    //a:=a+gamma*aB
    //b:=bA
    //b:=b+delta*bB
    //c_tmp := a * b
    //cA:=beta1*cA
    //cB:=beta2*cB
    //cA:=cA+alpha1*c_tmp
    //cB:=cB+alpha1*c_tmp


    //bli_printm( "aA", aA, "%4.1f", "" );
    //bli_printm( "aB", aB, "%4.1f", "" );
    //bli_printm( "gamma", gamma, "%4.1f", "" );

    mkl_copym( aA, &a );
    //bli_printm( "a", &a, "%4.1f", "" );

    if ( bli_obj_buffer_at_off( *aB ) != NULL ) {
        mkl_axpym ( gamma, aB, &a );
    }

    //bli_printm( "a", &a, "%4.1f", "" );
    //exit( 0 );

    mkl_copym( bA, &b );


    if ( bli_obj_buffer_at_off( *bB ) != NULL ) {
        mkl_axpym ( delta, bB, &b );
    }


    //bli_printm( "b", &b, "%4.1f", "" );



    mkl_gemm( &BLIS_ONE, &a, &b, &BLIS_ZERO, &c_tmp );

    //bli_printm( "cA", &cA, "%4.1f", "" );
    //bli_printm( "beta1", &beta1, "%4.1f", "" );

    //mkl_scalm( beta1, cA );
    mkl_axpym( alpha1, &c_tmp, cA );


    if ( bli_obj_buffer_at_off( *cB ) != NULL ) {
        //mkl_scalm( beta2, cB );
        mkl_axpym( alpha2, &c_tmp, cB );
    }
    //dtime_conventional = bli_clock_min_diff( dtime_conventional, dtime );

    bli_obj_free( &a );
    bli_obj_free( &b );
    bli_obj_free( &c_tmp );
}

void bli_mulstraprim_mkl_ref( dim_t   len,
                              obj_t**  alpha_list, 
                              obj_t**  a_list, obj_t**  gamma_list,
                              obj_t**  b_list, obj_t**  delta_list,
                              obj_t**  beta_list, 
                              obj_t**  c_list)
{

	num_t dt = bli_obj_datatype( **c_list );
    dim_t m  = bli_obj_length( **c_list );
    dim_t n  = bli_obj_width( **c_list );
    dim_t k  = bli_obj_width( **a_list );

    obj_t a, b, c_tmp;
    bli_obj_create( dt, m, k, 0, 0, &a );
    bli_obj_create( dt, k, n, 0, 0, &b );
    bli_obj_create( dt, m, n, 0, 0, &c_tmp );

    mkl_copym( a_list[ 0 ], &a );

    for ( dim_t i = 1; i < len; i++ ) {
        if( bli_obj_buffer( *a_list[ i ] ) != NULL ) {
            mkl_axpym( gamma_list[ i ], a_list[i], &a );
        }
    }

    mkl_copym( b_list[ 0 ], &b );

    for ( dim_t i = 1; i < len; i++ ) {
        if( bli_obj_buffer( *b_list[ i ] ) != NULL ) {
            mkl_axpym( delta_list[ i ], b_list[i], &b );
        }
    }

    mkl_gemm( &BLIS_ONE, &a, &b, &BLIS_ZERO, &c_tmp );

    mkl_axpym( alpha_list[0], &c_tmp, c_list[0] );

    for ( dim_t i = 1; i < len; i++ ) {
        if( bli_obj_buffer( *c_list[ i ] ) != NULL ) {
            mkl_axpym( alpha_list[ i ], &c_tmp, c_list[ i ] );
        }
    }

    bli_obj_free( &a );
    bli_obj_free( &b );
    bli_obj_free( &c_tmp );
}

void bli_straprim_hybrid( obj_t*  alpha1, obj_t*  alpha2,
                          obj_t*  aA, obj_t*  gamma, obj_t*  aB,
                          obj_t*  bA, obj_t*  delta, obj_t*  bB,
                          obj_t*  beta1, obj_t*  beta2,
                          obj_t*  cA, obj_t*  cB ) {

	num_t dt = bli_obj_datatype( *cA );
    dim_t m  = bli_obj_length( *cA );
    dim_t n  = bli_obj_width( *cA );
    dim_t k  = bli_obj_width( *aA );

    obj_t a, b, c_tmp;
    bli_obj_create( dt, m, k, 0, 0, &a );
    bli_obj_create( dt, k, n, 0, 0, &b );
    bli_obj_create( dt, m, n, 0, 0, &c_tmp );

    //dtime = bli_clock();
    //bli_straprim_ref( &alpha1, &alpha2,
    //                  &aA, &gamma, aB, 
    //                  &bA, &delta, &bB, 
    //                  &beta1, &beta2,
    //                  &cA_strassen, &cB_strassen );

    //cA = beta1*cA+alpha1*(aA+gamma*aB)(bA+delta*bB)
    //cB = beta2*cB+alpha2*(aA+gamma*aB)(bA+delta*bB) 
    //a:=aA
    //a:=a+gamma*aB
    //b:=bA
    //b:=b+delta*bB
    //c_tmp := a * b
    //cA:=beta1*cA
    //cB:=beta2*cB
    //cA:=cA+alpha1*c_tmp
    //cB:=cB+alpha1*c_tmp

    mkl_copym( aA, &a );

    if ( bli_obj_buffer( *aB ) != NULL ) {
        mkl_axpym ( gamma, aB, &a );
    }

    mkl_copym( bA, &b );

    if ( bli_obj_buffer( *bB ) != NULL ) {
        mkl_axpym ( delta, bB, &b );
    }

    bli_setm( &BLIS_ZERO, &c_tmp ); //We have to set c_tmp to zero before we call one_step_strassen
    //bli_gemm( &BLIS_ONE, &a, &b, &BLIS_ZERO, &c_tmp );
    bli_stra_1level( &a, &b, &c_tmp );

    //bli_printm( "cA", &cA, "%4.1f", "" );
    //bli_printm( "beta1", &beta1, "%4.1f", "" );

    //mkl_scalm( beta1, cA );
    mkl_axpym( alpha1, &c_tmp, cA );


    if ( bli_obj_buffer( *cB ) != NULL ) {
        //mkl_scalm( beta2, cB );
        mkl_axpym( alpha2, &c_tmp, cB );
    }
    //dtime_conventional = bli_clock_min_diff( dtime_conventional, dtime );

    bli_obj_free( &a );
    bli_obj_free( &b );
    bli_obj_free( &c_tmp );
}


