/*

   BLIS    
   An object-based framework for developing high-performance BLAS-like
   libraries.

   Copyright (C) 2014, The University of Texas at Austin

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are
   met:
    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    - Neither the name of The University of Texas at Austin nor the names
      of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
   HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "blis.h"

void bli_mulstraprim_blk_var2f( dim_t   len,
                                obj_t**  alpha_list, 
                                obj_t**  a_list, obj_t**  gamma_list,
                                obj_t**  b_list, obj_t**  delta_list,
                                obj_t**  beta_list, 
                                obj_t**  c_list,
                                gemm_t* cntl,
                                gemm_thrinfo_t* thread )
{
	obj_t a_pack_s[STRA_LIST_LEN];
    obj_t b1_pack_s[STRA_LIST_LEN], c1_pack_s[STRA_LIST_LEN];

    obj_t b1[STRA_LIST_LEN], c1[STRA_LIST_LEN];
    //obj_t*  aA_pack = NULL,  *aB_pack = NULL;
	//obj_t*  b1A_pack = NULL, *b1B_pack = NULL;
	//obj_t*  c1A_pack = NULL, *c1B_pack = NULL;
    obj_t*  a_pack[STRA_LIST_LEN] = {NULL};
	obj_t*  b1_pack[STRA_LIST_LEN] = {NULL};
	obj_t*  c1_pack[STRA_LIST_LEN] = {NULL};


	dim_t i;
	dim_t b_alg;

    if( thread_am_ochief( thread ) ) {
        // Initialize object for packing A
        #pragma unroll
        for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
            bli_obj_init_pack( &(a_pack_s[ ii ]) );
            bli_packm_init( a_list[ ii ], &a_pack_s[ ii ],
                        cntl_sub_packm_a( cntl ) );
        }

        // Scale C by beta (if instructed).
        bli_scalm_int( &BLIS_ONE,
                       c_list[ 0 ],
                       cntl_sub_scalm( cntl ) );
         
        #pragma unroll
        for ( dim_t ii = 1; ii < STRA_LIST_LEN; ii++ ) {
            if ( bli_obj_buffer( *c_list[ ii ] ) != NULL ) {
                bli_scalm_int( &BLIS_ONE,
                        c_list[ ii ],
                        cntl_sub_scalm( cntl ) );
            }
        }
    }

    #pragma unroll
    for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
        a_pack[ ii ] = thread_obroadcast( thread, &a_pack_s[ ii ] );
    }

	// Initialize pack objects for B and C that are passed into packm_init().
    if( thread_am_ichief( thread ) ) {
        #pragma unroll
        for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
            bli_obj_init_pack( &(b1_pack_s[ ii ]) );
            bli_obj_init_pack( &(c1_pack_s[ ii ]) );
        }
    }
    #pragma unroll
    for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
        b1_pack[ ii ] = thread_ibroadcast( thread, &(b1_pack_s[ ii ]) );
        c1_pack[ ii ] = thread_ibroadcast( thread, &(c1_pack_s[ ii ]) );
    }


	// Pack A (if instructed).
    #pragma unroll
    for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
        bli_packm_int( a_list[ ii ], a_pack[ ii ],
                cntl_sub_packm_a( cntl ),
                gemm_thread_sub_opackm( thread ) );
    }


    dim_t my_start, my_end;
    // Get range from bA (no need to get again from bB)
    bli_get_range_l2r( thread, b_list[ 0 ],
                       bli_blksz_get_mult_for_obj( b_list[ 0 ], cntl_blocksize( cntl ) ),
                       &my_start, &my_end );

	// Partition along the n dimension.
	for ( i = my_start; i < my_end; i += b_alg )
	{
		// Determine the current algorithmic blocksize.
		// NOTE: Use of b (for execution datatype) is intentional!
		// This causes the right blocksize to be used if c and a are
		// complex and b is real.
		b_alg = bli_determine_blocksize_f( i, my_end, b_list[ 0 ],
		                                   cntl_blocksize( cntl ) );

		// Acquire partitions for B1 and C1.
        #pragma unroll
        for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
            bli_acquire_mpart_l2r( BLIS_SUBPART1,
		                       i, b_alg, b_list[ ii ], &(b1[ ii ]) );
            bli_acquire_mpart_l2r( BLIS_SUBPART1,
		                       i, b_alg, c_list[ ii ], &(c1[ ii ]) );
        }
		
		// Initialize objects for packing A1 and B1.
        if( thread_am_ichief( thread ) ) {
            #pragma unroll
            for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
                bli_packm_init( &(b1[ ii ]), b1_pack[ ii ],
                            cntl_sub_packm_b( cntl ) );
                bli_packm_init( &(c1[ ii ]), c1_pack[ ii ],
                            cntl_sub_packm_c( cntl ) );
            }
        }
        thread_ibarrier( thread );

		// Pack B1 (if instructed).
        #pragma unroll
        for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
            bli_packm_int( &(b1[ ii ]), b1_pack[ ii ],
		               cntl_sub_packm_b( cntl ),
                       gemm_thread_sub_ipackm( thread ) );
        }


		// Pack C1 (if instructed).
        #pragma unroll
        for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
            bli_packm_int( &(c1[ ii ]), c1_pack[ ii ],
		               cntl_sub_packm_c( cntl ),
                       gemm_thread_sub_ipackm( thread ) );
        }

		// Perform mulstraprim subproblem.
		bli_mulstraprim_int( len,
                             alpha_list,
                             a_pack, gamma_list,
                             b1_pack, delta_list,
                             beta_list,
                             c1_pack,
                             cntl_sub_gemm( cntl ),
                             gemm_thread_sub_gemm( thread ) );

        thread_ibarrier( thread );

        // Unpack C1 (if C1 was packed).
        // Currently must be done by 1 thread
        #pragma unroll
        for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
            bli_unpackm_int( c1_pack[ ii ], &(c1[ ii ]),
                         cntl_sub_unpackm_c( cntl ),
                         gemm_thread_sub_ipackm( thread ) );
        }

	}

	// If any packing buffers were acquired within packm, release them back
	// to the memory manager.
    thread_obarrier( thread );
    if( thread_am_ochief( thread ) )
        #pragma unroll
        for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
            bli_packm_release( a_pack[ ii ], cntl_sub_packm_a( cntl ) );
        }
    if( thread_am_ichief( thread ) ) {
        #pragma unroll
        for ( dim_t ii = 0; ii < STRA_LIST_LEN; ii++ ) {
            bli_packm_release( b1_pack[ ii ], cntl_sub_packm_b( cntl ) );
            bli_packm_release( c1_pack[ ii ], cntl_sub_packm_c( cntl ) );
        }

    }
}

