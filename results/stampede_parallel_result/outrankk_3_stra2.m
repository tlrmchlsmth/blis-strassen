data_gemm_blis3(  1, 1:7 ) = [  200   1.486e-01 172.294  1.078e-09  9.893e-01 25.876  1.078e-09 ];
data_gemm_blis3(  2, 1:7 ) = [  400   3.007e-01 170.259  1.627e-08  1.076e+00 47.575  1.627e-08 ];
data_gemm_blis3(  3, 1:7 ) = [  600   4.544e-01 169.026  1.257e-08  1.197e+00 64.181  1.257e-08 ];
data_gemm_blis3(  4, 1:7 ) = [  800   6.250e-01 163.839  1.190e-08  1.325e+00 77.300  1.190e-08 ];
data_gemm_blis3(  5, 1:7 ) = [ 1000   7.463e-01 171.506  3.514e-08  1.462e+00 87.549  3.514e-08 ];
data_gemm_blis3(  6, 1:7 ) = [ 1200   8.977e-01 171.108  3.189e-08  1.754e+00 87.580  3.189e-08 ];
data_gemm_blis3(  7, 1:7 ) = [ 1400   1.050e+00 170.694  5.141e-08  1.850e+00 96.862  5.141e-08 ];
data_gemm_blis3(  8, 1:7 ) = [ 1600   1.206e+00 169.842  1.458e-07  2.063e+00 99.273  1.458e-07 ];
data_gemm_blis3(  9, 1:7 ) = [ 1800   1.374e+00 167.694  1.510e-07  2.209e+00 104.308  1.510e-07 ];
data_gemm_blis3( 10, 1:7 ) = [ 2000   1.487e+00 172.126  7.028e-08  2.358e+00 108.555  7.028e-08 ];
data_gemm_blis3( 11, 1:7 ) = [ 2200   1.641e+00 171.635  2.538e-08  2.653e+00 106.128  2.538e-08 ];
data_gemm_blis3( 12, 1:7 ) = [ 2400   1.793e+00 171.300  4.425e-08  2.758e+00 111.393  4.425e-08 ];
data_gemm_blis3( 13, 1:7 ) = [ 2600   1.953e+00 170.383  2.968e-07  2.901e+00 114.733  2.968e-07 ];
data_gemm_blis3( 14, 1:7 ) = [ 2800   2.084e+00 171.990  1.406e-07  3.043e+00 117.794  1.406e-07 ];
data_gemm_blis3( 15, 1:7 ) = [ 3000   2.231e+00 172.102  7.523e-07  3.199e+00 120.043  7.523e-07 ];
data_gemm_blis3( 16, 1:7 ) = [ 3200   2.387e+00 171.579  1.471e-07  3.482e+00 117.633  1.471e-07 ];
data_gemm_blis3( 17, 1:7 ) = [ 3400   2.537e+00 171.535  9.137e-07  3.601e+00 120.865  9.137e-07 ];
data_gemm_blis3( 18, 1:7 ) = [ 3600   2.710e+00 170.057  8.668e-07  3.725e+00 123.708  8.668e-07 ];
data_gemm_blis3( 19, 1:7 ) = [ 3800   2.830e+00 171.889  5.284e-07  3.875e+00 125.524  5.284e-07 ];
data_gemm_blis3( 20, 1:7 ) = [ 4000   2.980e+00 171.798  2.603e-08  4.018e+00 127.422  2.603e-08 ];
data_gemm_blis3( 21, 1:7 ) = [ 4200   3.132e+00 171.657  6.716e-07  4.324e+00 124.343  6.716e-07 ];
data_gemm_blis3( 22, 1:7 ) = [ 4400   3.292e+00 171.101  1.159e-07  4.414e+00 127.582  1.159e-07 ];
data_gemm_blis3( 23, 1:7 ) = [ 4600   3.426e+00 171.878  2.239e-07  4.559e+00 129.149  2.239e-07 ];
data_gemm_blis3( 24, 1:7 ) = [ 4800   3.576e+00 171.833  1.666e-07  4.699e+00 130.744  1.666e-07 ];
data_gemm_blis3( 25, 1:7 ) = [ 5000   3.731e+00 171.557  4.425e-07  4.860e+00 131.675  4.425e-07 ];
data_gemm_blis3( 26, 1:7 ) = [ 5200   3.882e+00 171.472  7.653e-07  5.146e+00 129.342  7.653e-07 ];
data_gemm_blis3( 27, 1:7 ) = [ 5400   4.049e+00 170.698  1.291e-06  5.259e+00 131.442  1.291e-06 ];
data_gemm_blis3( 28, 1:7 ) = [ 5600   4.168e+00 171.997  1.630e-06  5.385e+00 133.117  1.630e-06 ];
data_gemm_blis3( 29, 1:7 ) = [ 5800   4.323e+00 171.752  8.122e-07  5.526e+00 134.352  8.122e-07 ];
data_gemm_blis3( 30, 1:7 ) = [ 6000   4.474e+00 171.653  7.684e-06  5.663e+00 135.608  7.684e-06 ];
data_gemm_blis3( 31, 1:7 ) = [ 6200   4.626e+00 171.570  6.300e-06  5.992e+00 132.447  6.300e-06 ];
data_gemm_blis3( 32, 1:7 ) = [ 6400   4.768e+00 171.817  4.404e-06  6.097e+00 134.351  4.404e-06 ];
data_gemm_blis3( 33, 1:7 ) = [ 6600   4.915e+00 171.879  1.697e-06  6.211e+00 136.020  1.697e-06 ];
data_gemm_blis3( 34, 1:7 ) = [ 6800   5.067e+00 171.766  1.322e-06  6.366e+00 136.736  1.322e-06 ];
data_gemm_blis3( 35, 1:7 ) = [ 7000   5.222e+00 171.591  9.215e-07  6.507e+00 137.691  9.215e-07 ];
data_gemm_blis3( 36, 1:7 ) = [ 7200   5.385e+00 171.156  4.165e-07  6.823e+00 135.070  4.165e-07 ];
data_gemm_blis3( 37, 1:7 ) = [ 7400   5.512e+00 171.858  2.056e-07  6.948e+00 136.332  2.056e-07 ];
data_gemm_blis3( 38, 1:7 ) = [ 7600   5.661e+00 171.851  1.809e-06  7.055e+00 137.894  1.809e-06 ];
data_gemm_blis3( 39, 1:7 ) = [ 7800   5.813e+00 171.743  9.215e-07  7.189e+00 138.886  9.215e-07 ];
data_gemm_blis3( 40, 1:7 ) = [ 8000   5.968e+00 171.570  4.019e-06  7.316e+00 139.976  4.019e-06 ];
 
