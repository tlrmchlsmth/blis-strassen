data_gemm_blis5(  1, 1:7 ) = [  256   6.362e-03 21.097  2.362e-12  3.366e-02  3.987  5.731e-12 ];
data_gemm_blis5(  2, 1:7 ) = [  512   2.506e-02 21.424  4.822e-12  6.791e-02  7.906  1.094e-11 ];
data_gemm_blis5(  3, 1:7 ) = [  768   5.504e-02 21.947  9.013e-12  1.115e-01 10.837  1.729e-11 ];
data_gemm_blis5(  4, 1:7 ) = [ 1024   9.760e-02 22.003  1.306e-11  1.733e-01 12.390  2.407e-11 ];
data_gemm_blis5(  5, 1:7 ) = [ 1280   1.518e-01 22.104  1.730e-11  2.490e-01 13.474  3.033e-11 ];
data_gemm_blis5(  6, 1:7 ) = [ 1536   2.173e-01 22.231  2.078e-11  3.400e-01 14.210  3.660e-11 ];
data_gemm_blis5(  7, 1:7 ) = [ 1792   2.931e-01 22.439  2.716e-11  4.438e-01 14.818  4.448e-11 ];
data_gemm_blis5(  8, 1:7 ) = [ 2048   3.880e-01 22.138  3.203e-11  5.780e-01 14.862  5.241e-11 ];
data_gemm_blis5(  9, 1:7 ) = [ 2304   4.823e-01 22.540  3.694e-11  7.087e-01 15.340  5.750e-11 ];
data_gemm_blis5( 10, 1:7 ) = [ 2560   5.996e-01 22.383  4.056e-11  8.660e-01 15.499  6.519e-11 ];
data_gemm_blis5( 11, 1:7 ) = [ 2816   7.191e-01 22.585  4.866e-11  1.038e+00 15.645  7.374e-11 ];
data_gemm_blis5( 12, 1:7 ) = [ 3072   8.685e-01 22.253  5.554e-11  1.216e+00 15.893  8.517e-11 ];
data_gemm_blis5( 13, 1:7 ) = [ 3328   1.003e+00 22.608  6.163e-11  1.407e+00 16.118  8.786e-11 ];
data_gemm_blis5( 14, 1:7 ) = [ 3584   1.178e+00 22.329  6.799e-11  1.610e+00 16.340  9.824e-11 ];
data_gemm_blis5( 15, 1:7 ) = [ 3840   1.336e+00 22.601  7.039e-11  1.824e+00 16.553  1.003e-10 ];
data_gemm_blis5( 16, 1:7 ) = [ 4096   1.536e+00 22.366  8.607e-11  1.974e+00 17.407  1.186e-10 ];
data_gemm_blis5( 17, 1:7 ) = [ 4352   1.727e+00 22.457  8.915e-11  2.292e+00 16.921  1.225e-10 ];
data_gemm_blis5( 18, 1:7 ) = [ 4608   1.959e+00 22.200  9.483e-11  2.540e+00 17.123  1.338e-10 ];
data_gemm_blis5( 19, 1:7 ) = [ 4864   2.153e+00 22.507  1.035e-10  2.806e+00 17.270  1.411e-10 ];
data_gemm_blis5( 20, 1:7 ) = [ 5120   2.416e+00 22.222  1.093e-10  3.063e+00 17.528  1.493e-10 ];
data_gemm_blis5( 21, 1:7 ) = [ 5376   2.626e+00 22.536  1.247e-10  3.365e+00 17.592  1.595e-10 ];
data_gemm_blis5( 22, 1:7 ) = [ 5632   2.914e+00 22.291  1.252e-10  3.662e+00 17.741  1.657e-10 ];
data_gemm_blis5( 23, 1:7 ) = [ 5888   3.147e+00 22.558  1.412e-10  3.980e+00 17.840  1.814e-10 ];
data_gemm_blis5( 24, 1:7 ) = [ 6144   3.484e+00 22.189  1.439e-10  4.266e+00 18.122  1.872e-10 ];
data_gemm_blis5( 25, 1:7 ) = [ 6400   3.717e+00 22.569  1.573e-10  4.647e+00 18.052  1.971e-10 ];
data_gemm_blis5( 26, 1:7 ) = [ 6656   4.079e+00 22.244  1.547e-10  4.986e+00 18.199  2.013e-10 ];
data_gemm_blis5( 27, 1:7 ) = [ 6912   4.331e+00 22.594  1.837e-10  5.366e+00 18.235  2.285e-10 ];
data_gemm_blis5( 28, 1:7 ) = [ 7168   4.731e+00 22.240  1.876e-10  5.727e+00 18.373  2.341e-10 ];
data_gemm_blis5( 29, 1:7 ) = [ 7424   4.997e+00 22.587  2.021e-10  6.142e+00 18.379  2.497e-10 ];
data_gemm_blis5( 30, 1:7 ) = [ 7680   5.423e+00 22.273  2.012e-10  6.549e+00 18.444  2.484e-10 ];
data_gemm_blis5( 31, 1:7 ) = [ 7936   5.720e+00 22.548  2.034e-10  6.985e+00 18.467  2.531e-10 ];
data_gemm_blis5( 32, 1:7 ) = [ 8192   6.193e+00 22.192  2.281e-10  7.225e+00 19.021  2.780e-10 ];
