%stra_rank_k_0naive;
stra_rank_k_1macro;
stra_rank_k_2packing;
stra_rank_k_3micro;
stra_rank_k_4final;

% ---------------------------------------------------------
% Plotting
% ---------------------------------------------------------
figure;
% hFig = figure(1);
%set(hFig, 'Position', [0 0 160 240])

set( gcf, 'PaperSize', [3 3]);
set( gcf, 'PaperPosition', [0.25 0.25 3 3] );
set( gcf, 'Position', [0 0 600 400]);
%set( gcf, 'Position', [0 0 600*2 400*2]);


hold;

plot( data_gemm_blis1( :, 1 ), data_gemm_blis1( :, 3), '.-', 'LineWidth', 2, 'Color',  'c');

plot( data_gemm_blis1( :, 1 ), data_gemm_blis1( :, 6), '.-', 'LineWidth', 2, 'Color',  [0 0.2 1.0] );
plot( data_gemm_blis2( :, 1 ), data_gemm_blis2( :, 6), '.-', 'LineWidth', 2, 'Color', [1 0 0.2] );
plot( data_gemm_blis3( :, 1 ), data_gemm_blis3( :, 6), '.-', 'LineWidth', 2, 'Color',  'm' );

plot( data_gemm_blis4( :, 1 ), data_gemm_blis4( :, 6), '.-', 'LineWidth', 2, 'Color',  'g' );




xlabel( 'k' );
ylabel( 'Practical GFLOPS (2n^3/time)' );
title( 'Strassen(m=n=4000, k vary)' );

grid on;
axis square;
axis( [ 0 4000 0 30 ] );
%axis( [ 0 5000 0 248 ] );

ax = gca;
ax.YTick = [  0, 5, 10, 15, 20, 25, 28.32, 30 ];
%ax.YTick = [  0, 50, 100, 150, 200, 248];

%ax.XTick = [ 0, 200, 400, 600, 800, 1000];
ax.XTick = [ 0, 1000, 2000, 3000, 4000 ];
set( gca,'FontSize',16 );

legend( 'original BLIS', ...
        'macro', ...
        'packing', ...
        'micro', ...
        'assembly', ...
        'Location','SouthEast');
