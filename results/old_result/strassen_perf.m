stra_0naive;
stra_1macro;
stra_2packing;
stra_3micro;
stra_4final;

stra_6_twolevel_naive;

% ---------------------------------------------------------
% Plotting
% ---------------------------------------------------------
figure;
% hFig = figure(1);
%set(hFig, 'Position', [0 0 160 240])

set( gcf, 'PaperSize', [3 3]);
set( gcf, 'PaperPosition', [0.25 0.25 3 3] );
set( gcf, 'Position', [0 0 600 400]);
%set( gcf, 'Position', [0 0 600*2 400*2]);


hold;

%plot( run_step1_st( :, 1 ), run_step1_st( :,4), '.-', 'LineWidth', 2, 'Color',  [0 0.2 1.0] );
%plot( run_step1_st( :, 1 ), run_step1_st( :, 5), '.-', 'LineWidth', 2, 'Color', [1 0 0.2] );

%plot( run_step3_st( :, 1 ), run_step3_st( :,4), '.-', 'LineWidth', 2, 'Color',  [0 0.2 1.0] );
%plot( run_step3_st( :, 1 ), run_step3_st( :, 5), '.-', 'LineWidth', 2, 'Color', [1 0 0.2] );

%plot( run_step_final_st( :, 1 ), run_step_final_st( :,4), '.-', 'LineWidth', 2, 'Color',  [0 0.2 1.0] );
%plot( run_step_final_st( :, 1 ), run_step_final_st( :, 5), '.-', 'LineWidth', 2, 'Color', [1 0 0.2] );



%plot( run_step1_mt( :, 1 ), run_step1_mt( :,4), '.-', 'LineWidth', 2, 'Color',  [0 0.2 1.0] );
%plot( run_step1_mt( :, 1 ), run_step1_mt( :, 5), '.-', 'LineWidth', 2, 'Color', [1 0 0.2] );

%plot( run_step3_mt( :, 1 ), run_step3_mt( :,4), '.-', 'LineWidth', 2, 'Color',  [0 0.2 1.0] );
%plot( run_step3_mt( :, 1 ), run_step3_mt( :, 5), '.-', 'LineWidth', 2, 'Color', [1 0 0.2] );

plot( data_gemm_blis6( :, 1 ), data_gemm_blis6( :, 3), '.-', 'LineWidth', 2, 'Color',  'c');

plot( data_gemm_blis1( :, 1 ), data_gemm_blis1( :, 6), '.-', 'LineWidth', 2, 'Color',  [0 0.2 1.0] );
plot( data_gemm_blis2( :, 1 ), data_gemm_blis2( :, 6), '.-', 'LineWidth', 2, 'Color', [1 0 0.2] );
plot( data_gemm_blis3( :, 1 ), data_gemm_blis3( :, 6), '.-', 'LineWidth', 2, 'Color',  'm' );

plot( data_gemm_blis4( :, 1 ), data_gemm_blis4( :, 6), '.-', 'LineWidth', 2, 'Color',  'g' );

plot( data_gemm_blis6( :, 1 ), data_gemm_blis6( :, 6), '.-', 'LineWidth', 2, 'Color',  'k' );






%plot( dgemm_openblas_st_data( :, 1 ), dgemm_openblas_st_data( :, 5), '.--', 'LineWidth', 2, 'Color', 'c' );
%plot( dgemm_dash_data( :, 1 ), dgemm_dash_data( :, 4), '.--', 'LineWidth', 2, 'Color', 'm' );

%plot( sgemm_cublas_data( :, 1 ), sgemm_cublas_data( :,4), '.-', 'LineWidth', 2, 'Color', [0 0.2 1.0] );
%plot( sgemm_openblas_mt_data( :, 1 ), sgemm_openblas_mt_data( :, 5), '.--', 'LineWidth', 2, 'Color', [1 0 0.2] );
%plot( sgemm_openblas_st_data( :, 1 ), sgemm_openblas_st_data( :, 5), '.--', 'LineWidth', 2, 'Color', [0.2 0 1.0] );

%plot( sgemm_cublas_data( :, 1 ), sgemm_cublas_data( :,4), '.-', 'LineWidth', 2, 'Color', [1 0 0.2] );
%plot( blis_fusion1( :, 3 ), blis_fusion1( :,7 ), '.-', 'LineWidth', 2, 'Color', [1 0 0.2] );
%%plot( blis_gemm1( :, 3 ), blis_fusion2( :,6), '.--', 'LineWidth', 2, 'Color', [0 0.2 1.0] );
%%plot( blis_gemm1( :, 3 ), blis_fusion2( :,7 ), '.--', 'LineWidth', 2, 'Color', [1 0 0.2] );
%plot( blis_gemm1( :, 3 ), blis_gemm1( :,6), '.--', 'LineWidth', 2, 'Color', [0 0.2 1.0] );
%plot( blis_gemm1( :, 3 ), blis_gemm1( :,7 ), '.--', 'LineWidth', 2, 'Color', [1 0 0.2] );


xlabel( 'm=k=n' );
ylabel( 'Practical GFLOPS (2n^3/time)' );
title( 'Strassen(m=k=n)' );

grid on;
axis square;
axis( [ 0 12000 0 35 ] );
%axis( [ 0 5000 0 248 ] );

ax = gca;
ax.YTick = [  0, 5, 10, 15, 20, 25, 28.32, 30 ];
%ax.YTick = [  0, 50, 100, 150, 200, 248];

%ax.XTick = [ 0, 200, 400, 600, 800, 1000];
%ax.XTick = [ 0, 1000, 2000, 3000, 4000 ];
ax.XTick = [ 0, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000];


set( gca,'FontSize',24 );

legend( 'original BLIS', ...
        'macro', ...
        'packing', ...
        'micro', ...
        'assembly', ...
        'Location','SouthEast');
