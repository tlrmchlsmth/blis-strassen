stampede_output_straprim_1level_1;


% ---------------------------------------------------------
% Machine setup
% ---------------------------------------------------------
clockrate = 3.1;
%clockrate = 3.10 * 10;
channels  = 5;
%alpha     = 2.2   / channels;
alpha     = 3   / channels;

%alpha2    = 13.91 / channels;
%alpha2    = 8.0   / channels;
%alpha3    = 4.0   / channels;

tau       = 1 / ( 8 * clockrate);

% ---------------------------------------------------------
% Problem setup
% ---------------------------------------------------------
%m  = 8192;
%n  = 8192;
k  = 4:4:2048;
m = k;
n = k;
%r  = 2048;

% ---------------------------------------------------------
% Setup the block size
% ---------------------------------------------------------
mc = 96;
nc = 4096;
kc = 256;

% Compute effective flops
%flops_effective  = m * n * ( 2 * k  + 1 );
flops_effective  = m .* n .* ( 2 * k );

% Other instructions
flops_all        = 2 .* m .* k + 2 .* n .* k + m .* n .* ( 2 * k + 2 );

% Compute the floating point operation time
Tf  = flops_all  * tau;


% Compute the memory operation time
TdBc   = 1 * n .* k;
TdAc   = 1 * m .* k  .* ceil( n / nc );
%TdCc   = 1 * ( ceil( k / kc ) -1 ) .* m .* n;
TdCc   = 1 * ( ceil( k / kc ) ) .* m .* n;
%TdB2c  = 2 * n;
%TdA2c  = 2 * ceil( n / nc ) * ( 1 * m );

% We can model L1 spilling problem here.
%TdDc   = epsilon * 2.0 * m * r * log2( r );

%Td     = TdBc  + TdAc  + TdCc  + TdB2c + TdA2c; 
Td     = TdBc  + TdAc  + TdCc;
%Tdvar6 = TdBc  + TdAc  + TdCc  + TdB2c + TdA2c + 1.0 * m * n;
%Tdvar6 = Td + 1.0 * m .* n;

Td_ref = TdBc  + TdAc  + TdCc + 4.0 * m .* k + 4.0 * k .* n + 5.0 * m .* n;%1.0 * m .* k + 1.0 * m .* k +  
%Load A1, Load A2, Store A; Load B1, Load B2, Store B; (Load A, Load B, Store C_tmp, Note here we don't need to load C_tmp); Load C_tmp; Load C1, Load C2, Store C1, Store C2;

% Only load A1, store A1 is in L3 cache, so not counted....
%Td_blis = 2.0 * TdBc  + 2.0 * TdAc  + 2 * TdCc + 2.0 * m .* n;
%Td_blis = 2.0 * TdBc  + 2.0 * TdAc  + 3 * TdCc;
%Td_blis = 3.0 * TdBc  + 3.0 * TdAc  + 3.0 * TdCc;
Td_blis = 3.0 * TdBc  + 3.0 * TdAc  + 4 * TdCc;
%  (Load A1, Load A2, Load B1, Load B2, Load C1, Load C2, Store C1, Store C2);


% ---------------------------------------------------------
% Compute the total time 
% ---------------------------------------------------------
T_ref      = Tf  + Td_ref * alpha;
T_blis  = Tf  + Td_blis * alpha;


% ---------------------------------------------------------
% Plotting
% ---------------------------------------------------------
figure;
set( gcf, 'PaperSize', [3 3]);
set( gcf, 'PaperPosition', [0.25 0.25 3 3] );
set( gcf, 'Position', [0 0 1200 800]);
%semilogx( k, flops_effective ./ T, '--', 'LineWidth', 1.3, 'Color', [0 0.4 1.0] );
plot( k, 0.9 * flops_effective ./ T_ref, '--', 'LineWidth', 1.3, 'Color', 'b' );
hold on;
plot( k, flops_effective  ./ T_blis, '--', 'LineWidth', 1.3, 'Color', 'r' );
%hold on;
plot( straprim1( :, 1 ), straprim1( :,5), '.-', 'LineWidth', 1.3, 'Color',  [0 0.2 1.0] );
%hold on;
plot( straprim1( :, 1 ), straprim1( :, 7), '.-', 'LineWidth', 1.3, 'Color', [1 0 0.2] );

hold on;


xlabel( 'm=n=k' );
ylabel( 'GFLOPS' );
%title( 'Var#6, p=10, m=n=8192, k=2048' );
%title( 'Variant#6, nthd=10, m=n=8192, k=2048' );
title( 'p=1, m=n=k' );
grid on;
%axis square;
axis( [ 0 2048 0 30 ] );
%axis( [ 0 1028 0 248 ] );
ax = gca;
ax.YTick = [ 0, 5 , 10, 15, 20, 23.76, 25, 30 ];
%ax.YTick = [ 0 , 50 , 100, 150, 200, 248 ];
ax.XTick = [ 0, 200, 400, 600, 800, 1000, 1200, 1400, 1600, 1800, 2000 ];
set( gca,'FontSize',14 );

%legend( 'Modeled GSRNN Variant#6', ...
%        'Modeled MKL+STL', ...
%        'GSRNN Variant#6', ...
%        'MKL+STL', ...
%        'Location','SouthEast');
