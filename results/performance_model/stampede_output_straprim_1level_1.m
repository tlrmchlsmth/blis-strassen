% straprim1(  1, 1:5 ) = [    8   1.200e-05  0.085  8.000e-06  0.128 ];
% straprim1(  2, 1:5 ) = [   16   1.300e-05  0.630  9.000e-06  0.910 ];
% straprim1(  3, 1:5 ) = [   24   2.000e-05  1.382  1.200e-05  2.304 ];
% straprim1(  4, 1:5 ) = [   32   2.300e-05  2.849  1.400e-05  4.681 ];
% straprim1(  5, 1:5 ) = [   40   3.000e-05  4.267  1.900e-05  6.737 ];
% straprim1(  6, 1:5 ) = [   48   3.800e-05  5.821  2.700e-05  8.192 ];
% straprim1(  7, 1:5 ) = [   56   5.100e-05  6.887  3.600e-05  9.756 ];
% straprim1(  8, 1:5 ) = [   64   6.500e-05  8.066  4.700e-05 11.155 ];
% straprim1(  9, 1:5 ) = [   72   8.400e-05  8.887  6.000e-05 12.442 ];
% straprim1( 10, 1:5 ) = [   80   1.180e-04  8.678  7.900e-05 12.962 ];
% straprim1( 11, 1:5 ) = [   88   1.520e-04  8.967  9.700e-05 14.051 ];
% straprim1( 12, 1:5 ) = [   96   1.880e-04  9.412  1.210e-04 14.624 ];
% straprim1( 13, 1:5 ) = [  104   2.350e-04  9.573  1.480e-04 15.201 ];
% straprim1( 14, 1:5 ) = [  112   2.780e-04 10.107  1.950e-04 14.410 ];
% straprim1( 15, 1:5 ) = [  120   3.280e-04 10.537  2.250e-04 15.360 ];
% straprim1( 16, 1:5 ) = [  128   3.900e-04 10.755  2.500e-04 16.777 ];
% straprim1( 17, 1:5 ) = [  136   4.580e-04 10.985  2.930e-04 17.170 ];
% straprim1( 18, 1:5 ) = [  144   5.240e-04 11.397  3.430e-04 17.411 ];
% straprim1( 19, 1:5 ) = [  152   6.050e-04 11.609  3.900e-04 18.009 ];
% straprim1( 20, 1:5 ) = [  160   6.900e-04 11.872  4.490e-04 18.245 ];
% straprim1( 21, 1:5 ) = [  168   7.710e-04 12.300  5.140e-04 18.450 ];
% straprim1( 22, 1:5 ) = [  176   8.740e-04 12.475  5.840e-04 18.670 ];
% straprim1( 23, 1:5 ) = [  184   9.740e-04 12.792  6.560e-04 18.992 ];
% straprim1( 24, 1:5 ) = [  192   1.093e-03 12.951  7.390e-04 19.155 ];
% straprim1( 25, 1:5 ) = [  200   1.214e-03 13.180  8.300e-04 19.277 ];
% straprim1( 26, 1:5 ) = [  208   1.343e-03 13.401  9.320e-04 19.311 ];
% straprim1( 27, 1:5 ) = [  216   1.481e-03 13.609  1.035e-03 19.474 ];
% straprim1( 28, 1:5 ) = [  224   1.625e-03 13.833  1.150e-03 19.547 ];
% straprim1( 29, 1:5 ) = [  232   1.780e-03 14.031  1.266e-03 19.727 ];
% straprim1( 30, 1:5 ) = [  240   1.961e-03 14.099  1.393e-03 19.848 ];
% straprim1( 31, 1:5 ) = [  248   2.215e-03 13.772  1.530e-03 19.939 ];
% straprim1( 32, 1:5 ) = [  256   2.432e-03 13.797  1.708e-03 19.645 ];
% straprim1( 33, 1:5 ) = [  264   2.717e-03 13.544  1.917e-03 19.196 ];
% straprim1( 34, 1:5 ) = [  272   2.937e-03 13.704  2.083e-03 19.322 ];
% straprim1( 35, 1:5 ) = [  280   3.175e-03 13.828  2.256e-03 19.461 ];
% straprim1( 36, 1:5 ) = [  288   3.426e-03 13.945  2.448e-03 19.516 ];
% straprim1( 37, 1:5 ) = [  296   3.671e-03 14.129  2.653e-03 19.551 ];
% straprim1( 38, 1:5 ) = [  304   3.945e-03 14.243  2.861e-03 19.640 ];
% straprim1( 39, 1:5 ) = [  312   4.236e-03 14.340  3.073e-03 19.767 ];
% straprim1( 40, 1:5 ) = [  320   4.517e-03 14.509  3.309e-03 19.805 ];
% straprim1( 41, 1:5 ) = [  328   4.834e-03 14.600  3.541e-03 19.931 ];
% straprim1( 42, 1:5 ) = [  336   5.151e-03 14.728  3.799e-03 19.970 ];
% straprim1( 43, 1:5 ) = [  344   5.496e-03 14.814  4.061e-03 20.048 ];
% straprim1( 44, 1:5 ) = [  352   5.805e-03 15.026  4.330e-03 20.145 ];
% straprim1( 45, 1:5 ) = [  360   6.152e-03 15.168  4.618e-03 20.206 ];
% straprim1( 46, 1:5 ) = [  368   6.471e-03 15.403  4.914e-03 20.283 ];
% straprim1( 47, 1:5 ) = [  376   6.868e-03 15.480  5.223e-03 20.355 ];
% straprim1( 48, 1:5 ) = [  384   7.157e-03 15.823  5.564e-03 20.353 ];
% straprim1( 49, 1:5 ) = [  392   7.417e-03 16.243  5.901e-03 20.416 ];
% straprim1( 50, 1:5 ) = [  400   7.852e-03 16.302  6.250e-03 20.480 ];
% straprim1( 51, 1:5 ) = [  408   8.295e-03 16.375  6.607e-03 20.559 ];
% straprim1( 52, 1:5 ) = [  416   8.836e-03 16.295  7.033e-03 20.472 ];
% straprim1( 53, 1:5 ) = [  424   9.275e-03 16.437  7.410e-03 20.574 ];
% straprim1( 54, 1:5 ) = [  432   9.854e-03 16.363  7.869e-03 20.491 ];
% straprim1( 55, 1:5 ) = [  440   1.057e-02 16.115  8.299e-03 20.529 ];
% straprim1( 56, 1:5 ) = [  448   1.102e-02 16.317  8.762e-03 20.524 ];
% straprim1( 57, 1:5 ) = [  456   1.165e-02 16.277  9.265e-03 20.468 ];
% straprim1( 58, 1:5 ) = [  464   1.244e-02 16.063  9.768e-03 20.454 ];
% straprim1( 59, 1:5 ) = [  472   1.304e-02 16.129  1.031e-02 20.395 ];
% straprim1( 60, 1:5 ) = [  480   1.368e-02 16.171  1.086e-02 20.363 ];
% straprim1( 61, 1:5 ) = [  488   1.437e-02 16.170  1.142e-02 20.346 ];
% straprim1( 62, 1:5 ) = [  496   1.506e-02 16.208  1.202e-02 20.302 ];
% straprim1( 63, 1:5 ) = [  504   1.574e-02 16.269  1.267e-02 20.212 ];
% straprim1( 64, 1:5 ) = [  512   1.651e-02 16.259  1.340e-02 20.031 ];
% straprim1( 65, 1:5 ) = [  520   1.752e-02 16.053  1.408e-02 19.966 ];
% straprim1( 66, 1:5 ) = [  528   1.818e-02 16.198  1.471e-02 20.008 ];
% straprim1( 67, 1:5 ) = [  536   1.893e-02 16.270  1.535e-02 20.064 ];
% straprim1( 68, 1:5 ) = [  544   1.969e-02 16.353  1.608e-02 20.027 ];
% straprim1( 69, 1:5 ) = [  552   2.049e-02 16.413  1.677e-02 20.060 ];
% straprim1( 70, 1:5 ) = [  560   2.138e-02 16.427  1.765e-02 19.903 ];
% straprim1( 71, 1:5 ) = [  568   2.216e-02 16.542  1.825e-02 20.086 ];
% straprim1( 72, 1:5 ) = [  576   2.306e-02 16.577  1.902e-02 20.097 ];
% straprim1( 73, 1:5 ) = [  584   2.394e-02 16.643  1.980e-02 20.120 ];
% straprim1( 74, 1:5 ) = [  592   2.486e-02 16.693  2.062e-02 20.128 ];
% straprim1( 75, 1:5 ) = [  600   2.579e-02 16.753  2.142e-02 20.165 ];
% straprim1( 76, 1:5 ) = [  608   2.668e-02 16.846  2.231e-02 20.152 ];
% straprim1( 77, 1:5 ) = [  616   2.759e-02 16.944  2.307e-02 20.264 ];
% straprim1( 78, 1:5 ) = [  624   2.856e-02 17.015  2.400e-02 20.248 ];
% straprim1( 79, 1:5 ) = [  632   2.959e-02 17.061  2.484e-02 20.322 ];
% straprim1( 80, 1:5 ) = [  640   3.063e-02 17.115  2.579e-02 20.326 ];
% straprim1( 81, 1:5 ) = [  648   3.163e-02 17.205  2.672e-02 20.366 ];
% straprim1( 82, 1:5 ) = [  656   3.274e-02 17.244  2.771e-02 20.374 ];
% straprim1( 83, 1:5 ) = [  664   3.382e-02 17.312  2.863e-02 20.451 ];
% straprim1( 84, 1:5 ) = [  672   3.499e-02 17.345  2.968e-02 20.451 ];
% straprim1( 85, 1:5 ) = [  680   3.620e-02 17.372  3.069e-02 20.493 ];
% straprim1( 86, 1:5 ) = [  688   3.736e-02 17.434  3.176e-02 20.510 ];
% straprim1( 87, 1:5 ) = [  696   3.861e-02 17.466  3.280e-02 20.558 ];
% straprim1( 88, 1:5 ) = [  704   3.983e-02 17.520  3.398e-02 20.536 ];
% straprim1( 89, 1:5 ) = [  712   4.105e-02 17.585  3.507e-02 20.586 ];
% straprim1( 90, 1:5 ) = [  720   4.229e-02 17.652  3.627e-02 20.583 ];
% straprim1( 91, 1:5 ) = [  728   4.362e-02 17.689  3.743e-02 20.619 ];
% straprim1( 92, 1:5 ) = [  736   4.495e-02 17.741  3.869e-02 20.609 ];
% straprim1( 93, 1:5 ) = [  744   4.657e-02 17.685  3.975e-02 20.722 ];
% straprim1( 94, 1:5 ) = [  752   4.773e-02 17.821  4.110e-02 20.693 ];
% straprim1( 95, 1:5 ) = [  760   4.918e-02 17.850  4.230e-02 20.755 ];
% straprim1( 96, 1:5 ) = [  768   5.094e-02 17.784  4.409e-02 20.549 ];
% straprim1( 97, 1:5 ) = [  776   5.282e-02 17.695  4.587e-02 20.376 ];
% straprim1( 98, 1:5 ) = [  784   5.427e-02 17.759  4.717e-02 20.430 ];
% straprim1( 99, 1:5 ) = [  792   5.583e-02 17.797  4.856e-02 20.460 ];
% straprim1( 100, 1:5 ) = [  800   5.735e-02 17.856  5.005e-02 20.458 ];
% straprim1( 101, 1:5 ) = [  808   5.895e-02 17.897  5.145e-02 20.507 ];
% straprim1( 102, 1:5 ) = [  816   6.047e-02 17.971  5.286e-02 20.556 ];
% straprim1( 103, 1:5 ) = [  824   6.211e-02 18.016  5.445e-02 20.550 ];
% straprim1( 104, 1:5 ) = [  832   6.394e-02 18.014  5.601e-02 20.566 ];
% straprim1( 105, 1:5 ) = [  840   6.543e-02 18.117  5.757e-02 20.590 ];
% straprim1( 106, 1:5 ) = [  848   6.717e-02 18.158  5.914e-02 20.621 ];
% straprim1( 107, 1:5 ) = [  856   6.902e-02 18.174  6.063e-02 20.690 ];
% straprim1( 108, 1:5 ) = [  864   7.073e-02 18.237  6.235e-02 20.689 ];
% straprim1( 109, 1:5 ) = [  872   7.263e-02 18.258  6.410e-02 20.689 ];
% straprim1( 110, 1:5 ) = [  880   7.442e-02 18.314  6.575e-02 20.730 ];
% straprim1( 111, 1:5 ) = [  888   7.644e-02 18.320  6.758e-02 20.723 ];
% straprim1( 112, 1:5 ) = [  896   7.828e-02 18.378  6.930e-02 20.759 ];
% straprim1( 113, 1:5 ) = [  904   8.025e-02 18.411  7.097e-02 20.819 ];
% straprim1( 114, 1:5 ) = [  912   8.216e-02 18.466  7.282e-02 20.834 ];
% straprim1( 115, 1:5 ) = [  920   8.428e-02 18.479  7.452e-02 20.897 ];
% straprim1( 116, 1:5 ) = [  928   8.630e-02 18.520  7.673e-02 20.830 ];
% straprim1( 117, 1:5 ) = [  936   8.841e-02 18.551  7.844e-02 20.908 ];
% straprim1( 118, 1:5 ) = [  944   9.043e-02 18.605  8.032e-02 20.948 ];
% straprim1( 119, 1:5 ) = [  952   9.266e-02 18.624  8.216e-02 21.002 ];
% straprim1( 120, 1:5 ) = [  960   9.484e-02 18.658  8.424e-02 21.006 ];
% straprim1( 121, 1:5 ) = [  968   9.715e-02 18.674  8.640e-02 20.996 ];
% straprim1( 122, 1:5 ) = [  976   9.931e-02 18.724  8.859e-02 20.988 ];
% straprim1( 123, 1:5 ) = [  984   1.016e-01 18.749  9.048e-02 21.061 ];
% straprim1( 124, 1:5 ) = [  992   1.039e-01 18.782  9.307e-02 20.978 ];
% straprim1( 125, 1:5 ) = [ 1000   1.064e-01 18.802  9.472e-02 21.115 ];
% straprim1( 126, 1:5 ) = [ 1008   1.089e-01 18.813  9.697e-02 21.124 ];
% straprim1( 127, 1:5 ) = [ 1016   1.116e-01 18.796  9.932e-02 21.120 ];
% straprim1( 128, 1:5 ) = [ 1024   1.144e-01 18.776  1.043e-01 20.599 ];
% straprim1( 129, 1:5 ) = [ 1032   1.196e-01 18.380  1.099e-01 20.010 ];
% straprim1( 130, 1:5 ) = [ 1040   1.220e-01 18.443  1.121e-01 20.062 ];
% straprim1( 131, 1:5 ) = [ 1048   1.241e-01 18.543  1.141e-01 20.176 ];
% straprim1( 132, 1:5 ) = [ 1056   1.265e-01 18.617  1.172e-01 20.094 ];
% straprim1( 133, 1:5 ) = [ 1064   1.292e-01 18.641  1.197e-01 20.126 ];
% straprim1( 134, 1:5 ) = [ 1072   1.318e-01 18.693  1.221e-01 20.177 ];
% straprim1( 135, 1:5 ) = [ 1080   1.347e-01 18.703  1.247e-01 20.202 ];
% straprim1( 136, 1:5 ) = [ 1088   1.374e-01 18.741  1.275e-01 20.208 ];
% straprim1( 137, 1:5 ) = [ 1096   1.403e-01 18.761  1.300e-01 20.252 ];
% straprim1( 138, 1:5 ) = [ 1104   1.432e-01 18.793  1.326e-01 20.293 ];
% straprim1( 139, 1:5 ) = [ 1112   1.470e-01 18.710  1.353e-01 20.319 ];
% straprim1( 140, 1:5 ) = [ 1120   1.500e-01 18.737  1.381e-01 20.353 ];
% straprim1( 141, 1:5 ) = [ 1128   1.530e-01 18.767  1.414e-01 20.308 ];
% straprim1( 142, 1:5 ) = [ 1136   1.561e-01 18.788  1.439e-01 20.380 ];
% straprim1( 143, 1:5 ) = [ 1144   1.592e-01 18.810  1.467e-01 20.416 ];
% straprim1( 144, 1:5 ) = [ 1152   1.623e-01 18.841  1.497e-01 20.423 ];
% straprim1( 145, 1:5 ) = [ 1160   1.655e-01 18.864  1.530e-01 20.404 ];
% straprim1( 146, 1:5 ) = [ 1168   1.686e-01 18.901  1.559e-01 20.438 ];
% straprim1( 147, 1:5 ) = [ 1176   1.718e-01 18.930  1.584e-01 20.534 ];
% straprim1( 148, 1:5 ) = [ 1184   1.751e-01 18.956  1.620e-01 20.494 ];
% straprim1( 149, 1:5 ) = [ 1192   1.789e-01 18.937  1.656e-01 20.457 ];
% straprim1( 150, 1:5 ) = [ 1200   1.820e-01 18.990  1.679e-01 20.578 ];
% straprim1( 151, 1:5 ) = [ 1208   1.852e-01 19.036  1.708e-01 20.643 ];
% straprim1( 152, 1:5 ) = [ 1216   1.888e-01 19.045  1.748e-01 20.577 ];
% straprim1( 153, 1:5 ) = [ 1224   1.924e-01 19.061  1.778e-01 20.631 ];
% straprim1( 154, 1:5 ) = [ 1232   1.960e-01 19.078  1.808e-01 20.689 ];
% straprim1( 155, 1:5 ) = [ 1240   1.992e-01 19.138  1.841e-01 20.708 ];
% straprim1( 156, 1:5 ) = [ 1248   2.031e-01 19.139  1.883e-01 20.648 ];
% straprim1( 157, 1:5 ) = [ 1256   2.072e-01 19.123  1.912e-01 20.722 ];
% straprim1( 158, 1:5 ) = [ 1264   2.106e-01 19.181  1.949e-01 20.727 ];
% straprim1( 159, 1:5 ) = [ 1272   2.142e-01 19.215  1.991e-01 20.677 ];
% straprim1( 160, 1:5 ) = [ 1280   2.187e-01 19.178  2.042e-01 20.538 ];
% straprim1( 161, 1:5 ) = [ 1288   2.238e-01 19.099  2.092e-01 20.425 ];
% straprim1( 162, 1:5 ) = [ 1296   2.278e-01 19.110  2.120e-01 20.535 ];
% straprim1( 163, 1:5 ) = [ 1304   2.317e-01 19.142  2.158e-01 20.550 ];
% straprim1( 164, 1:5 ) = [ 1312   2.358e-01 19.157  2.200e-01 20.529 ];
% straprim1( 165, 1:5 ) = [ 1320   2.396e-01 19.201  2.234e-01 20.591 ];
% straprim1( 166, 1:5 ) = [ 1328   2.436e-01 19.225  2.273e-01 20.610 ];
% straprim1( 167, 1:5 ) = [ 1336   2.478e-01 19.243  2.320e-01 20.557 ];
% straprim1( 168, 1:5 ) = [ 1344   2.521e-01 19.259  2.354e-01 20.628 ];
% straprim1( 169, 1:5 ) = [ 1352   2.566e-01 19.260  2.399e-01 20.606 ];
% straprim1( 170, 1:5 ) = [ 1360   2.606e-01 19.306  2.430e-01 20.700 ];
% straprim1( 171, 1:5 ) = [ 1368   2.650e-01 19.322  2.477e-01 20.668 ];
% straprim1( 172, 1:5 ) = [ 1376   2.693e-01 19.347  2.514e-01 20.724 ];
% straprim1( 173, 1:5 ) = [ 1384   2.739e-01 19.355  2.557e-01 20.734 ];
% straprim1( 174, 1:5 ) = [ 1392   2.780e-01 19.407  2.607e-01 20.691 ];
% straprim1( 175, 1:5 ) = [ 1400   2.824e-01 19.432  2.643e-01 20.763 ];
% straprim1( 176, 1:5 ) = [ 1408   2.871e-01 19.442  2.688e-01 20.771 ];
% straprim1( 177, 1:5 ) = [ 1416   2.919e-01 19.452  2.732e-01 20.784 ];
% straprim1( 178, 1:5 ) = [ 1424   2.963e-01 19.488  2.781e-01 20.768 ];
% straprim1( 179, 1:5 ) = [ 1432   3.012e-01 19.500  2.819e-01 20.834 ];
% straprim1( 180, 1:5 ) = [ 1440   3.060e-01 19.518  2.868e-01 20.826 ];
% straprim1( 181, 1:5 ) = [ 1448   3.110e-01 19.523  2.911e-01 20.858 ];
% straprim1( 182, 1:5 ) = [ 1456   3.155e-01 19.568  2.965e-01 20.821 ];
% straprim1( 183, 1:5 ) = [ 1464   3.205e-01 19.582  3.003e-01 20.900 ];
% straprim1( 184, 1:5 ) = [ 1472   3.251e-01 19.624  3.058e-01 20.862 ];
% straprim1( 185, 1:5 ) = [ 1480   3.305e-01 19.616  3.098e-01 20.926 ];
% straprim1( 186, 1:5 ) = [ 1488   3.353e-01 19.654  3.152e-01 20.904 ];
% straprim1( 187, 1:5 ) = [ 1496   3.406e-01 19.660  3.190e-01 20.990 ];
% straprim1( 188, 1:5 ) = [ 1504   3.456e-01 19.690  3.243e-01 20.981 ];
% straprim1( 189, 1:5 ) = [ 1512   3.509e-01 19.700  3.288e-01 21.023 ];
% straprim1( 190, 1:5 ) = [ 1520   3.561e-01 19.723  3.347e-01 20.983 ];
% straprim1( 191, 1:5 ) = [ 1528   3.622e-01 19.698  3.412e-01 20.910 ];
% straprim1( 192, 1:5 ) = [ 1536   3.690e-01 19.639  3.514e-01 20.627 ];
% straprim1( 193, 1:5 ) = [ 1544   3.762e-01 19.569  3.571e-01 20.614 ];
% straprim1( 194, 1:5 ) = [ 1552   3.810e-01 19.625  3.594e-01 20.806 ];
% straprim1( 195, 1:5 ) = [ 1560   3.856e-01 19.689  3.651e-01 20.796 ];
% straprim1( 196, 1:5 ) = [ 1568   3.917e-01 19.684  3.700e-01 20.837 ];
% straprim1( 197, 1:5 ) = [ 1576   3.971e-01 19.716  3.751e-01 20.869 ];
% straprim1( 198, 1:5 ) = [ 1584   4.028e-01 19.732  3.806e-01 20.887 ];
% straprim1( 199, 1:5 ) = [ 1592   4.089e-01 19.734  3.855e-01 20.933 ];
% straprim1( 200, 1:5 ) = [ 1600   4.142e-01 19.776  3.933e-01 20.827 ];
% straprim1( 201, 1:5 ) = [ 1608   4.203e-01 19.783  3.973e-01 20.928 ];
% straprim1( 202, 1:5 ) = [ 1616   4.263e-01 19.801  4.031e-01 20.938 ];
% straprim1( 203, 1:5 ) = [ 1624   4.318e-01 19.840  4.082e-01 20.988 ];
% straprim1( 204, 1:5 ) = [ 1632   4.377e-01 19.860  4.145e-01 20.975 ];
% straprim1( 205, 1:5 ) = [ 1640   4.442e-01 19.860  4.215e-01 20.930 ];
% straprim1( 206, 1:5 ) = [ 1648   4.507e-01 19.862  4.263e-01 21.000 ];
% straprim1( 207, 1:5 ) = [ 1656   4.567e-01 19.885  4.324e-01 21.006 ];
% straprim1( 208, 1:5 ) = [ 1664   4.634e-01 19.887  4.388e-01 20.998 ];
% straprim1( 209, 1:5 ) = [ 1672   4.690e-01 19.932  4.452e-01 20.998 ];
% straprim1( 210, 1:5 ) = [ 1680   4.757e-01 19.936  4.524e-01 20.964 ];
% straprim1( 211, 1:5 ) = [ 1688   4.821e-01 19.954  4.564e-01 21.077 ];
% straprim1( 212, 1:5 ) = [ 1696   4.889e-01 19.956  4.629e-01 21.077 ];
% straprim1( 213, 1:5 ) = [ 1704   4.958e-01 19.957  4.708e-01 21.021 ];
% straprim1( 214, 1:5 ) = [ 1712   5.031e-01 19.949  4.752e-01 21.118 ];
% straprim1( 215, 1:5 ) = [ 1720   5.095e-01 19.973  4.817e-01 21.127 ];
% straprim1( 216, 1:5 ) = [ 1728   5.159e-01 20.003  4.894e-01 21.088 ];
% straprim1( 217, 1:5 ) = [ 1736   5.236e-01 19.984  4.954e-01 21.119 ];
% straprim1( 218, 1:5 ) = [ 1744   5.301e-01 20.013  5.020e-01 21.133 ];
% straprim1( 219, 1:5 ) = [ 1752   5.368e-01 20.035  5.084e-01 21.154 ];
% straprim1( 220, 1:5 ) = [ 1760   5.444e-01 20.030  5.157e-01 21.143 ];
% straprim1( 221, 1:5 ) = [ 1768   5.514e-01 20.046  5.234e-01 21.119 ];
% straprim1( 222, 1:5 ) = [ 1776   5.586e-01 20.058  5.297e-01 21.149 ];
% straprim1( 223, 1:5 ) = [ 1784   5.665e-01 20.046  5.365e-01 21.166 ];
% straprim1( 224, 1:5 ) = [ 1792   5.729e-01 20.088  5.487e-01 20.974 ];
% straprim1( 225, 1:5 ) = [ 1800   5.831e-01 20.003  5.563e-01 20.966 ];
% straprim1( 226, 1:5 ) = [ 1808   5.901e-01 20.032  5.620e-01 21.033 ];
% straprim1( 227, 1:5 ) = [ 1816   5.972e-01 20.058  5.684e-01 21.073 ];
% straprim1( 228, 1:5 ) = [ 1824   6.054e-01 20.049  5.763e-01 21.060 ];
% straprim1( 229, 1:5 ) = [ 1832   6.128e-01 20.066  5.840e-01 21.057 ];
% straprim1( 230, 1:5 ) = [ 1840   6.205e-01 20.081  5.921e-01 21.044 ];
% straprim1( 231, 1:5 ) = [ 1848   6.284e-01 20.087  5.992e-01 21.066 ];
% straprim1( 232, 1:5 ) = [ 1856   6.361e-01 20.101  6.072e-01 21.060 ];
% straprim1( 233, 1:5 ) = [ 1864   6.442e-01 20.108  6.137e-01 21.106 ];
% straprim1( 234, 1:5 ) = [ 1872   6.525e-01 20.107  6.232e-01 21.054 ];
% straprim1( 235, 1:5 ) = [ 1880   6.602e-01 20.128  6.284e-01 21.146 ];
% straprim1( 236, 1:5 ) = [ 1888   6.673e-01 20.169  6.360e-01 21.163 ];
% straprim1( 237, 1:5 ) = [ 1896   6.756e-01 20.177  6.438e-01 21.174 ];
% straprim1( 238, 1:5 ) = [ 1904   6.834e-01 20.199  6.521e-01 21.170 ];
% straprim1( 239, 1:5 ) = [ 1912   6.915e-01 20.217  6.593e-01 21.202 ];
% straprim1( 240, 1:5 ) = [ 1920   6.995e-01 20.238  6.683e-01 21.182 ];
% straprim1( 241, 1:5 ) = [ 1928   7.089e-01 20.219  6.785e-01 21.127 ];
% straprim1( 242, 1:5 ) = [ 1936   7.176e-01 20.224  6.848e-01 21.193 ];
% straprim1( 243, 1:5 ) = [ 1944   7.266e-01 20.222  6.921e-01 21.231 ];
% straprim1( 244, 1:5 ) = [ 1952   7.351e-01 20.237  7.009e-01 21.222 ];
% straprim1( 245, 1:5 ) = [ 1960   7.443e-01 20.233  7.082e-01 21.265 ];
% straprim1( 246, 1:5 ) = [ 1968   7.524e-01 20.261  7.173e-01 21.253 ];
% straprim1( 247, 1:5 ) = [ 1976   7.609e-01 20.279  7.255e-01 21.269 ];
% straprim1( 248, 1:5 ) = [ 1984   7.712e-01 20.254  7.360e-01 21.220 ];
% straprim1( 249, 1:5 ) = [ 1992   7.791e-01 20.292  7.430e-01 21.278 ];
% straprim1( 250, 1:5 ) = [ 2000   7.882e-01 20.299  7.507e-01 21.315 ];
% straprim1( 251, 1:5 ) = [ 2008   8.018e-01 20.196  7.630e-01 21.223 ];
% straprim1( 252, 1:5 ) = [ 2016   8.112e-01 20.202  7.722e-01 21.220 ];
% straprim1( 253, 1:5 ) = [ 2024   8.200e-01 20.222  7.816e-01 21.217 ];
% straprim1( 254, 1:5 ) = [ 2032   8.306e-01 20.203  7.904e-01 21.230 ];
% straprim1( 255, 1:5 ) = [ 2040   8.409e-01 20.192  8.006e-01 21.209 ];
% straprim1( 256, 1:5 ) = [ 2048   8.475e-01 20.272  8.421e-01 20.401 ];



straprim1(  1, 1:7 ) = [    8    8    8   1.200e-05  0.085  8.000e-06  0.128 ];
straprim1(  2, 1:7 ) = [   16   16   16   1.200e-05  0.683  8.000e-06  1.024 ];
straprim1(  3, 1:7 ) = [   24   24   24   1.700e-05  1.626  1.000e-05  2.765 ];
straprim1(  4, 1:7 ) = [   32   32   32   4.100e-05  1.598  1.600e-05  4.096 ];
straprim1(  5, 1:7 ) = [   40   40   40   2.900e-05  4.414  1.900e-05  6.737 ];
straprim1(  6, 1:7 ) = [   48   48   48   3.800e-05  5.821  2.600e-05  8.507 ];
straprim1(  7, 1:7 ) = [   56   56   56   5.000e-05  7.025  3.500e-05 10.035 ];
straprim1(  8, 1:7 ) = [   64   64   64   6.400e-05  8.192  4.600e-05 11.398 ];
straprim1(  9, 1:7 ) = [   72   72   72   8.300e-05  8.994  6.000e-05 12.442 ];
straprim1( 10, 1:7 ) = [   80   80   80   1.160e-04  8.828  8.600e-05 11.907 ];
straprim1( 11, 1:7 ) = [   88   88   88   1.500e-04  9.086  1.050e-04 12.980 ];
straprim1( 12, 1:7 ) = [   96   96   96   1.880e-04  9.412  1.190e-04 14.870 ];
straprim1( 13, 1:7 ) = [  104  104  104   2.330e-04  9.655  1.470e-04 15.304 ];
straprim1( 14, 1:7 ) = [  112  112  112   2.790e-04 10.071  1.770e-04 15.875 ];
straprim1( 15, 1:7 ) = [  120  120  120   3.300e-04 10.473  2.110e-04 16.379 ];
straprim1( 16, 1:7 ) = [  128  128  128   3.930e-04 10.673  2.500e-04 16.777 ];
straprim1( 17, 1:7 ) = [  136  136  136   4.590e-04 10.961  2.920e-04 17.229 ];
straprim1( 18, 1:7 ) = [  144  144  144   5.310e-04 11.247  3.400e-04 17.565 ];
straprim1( 19, 1:7 ) = [  152  152  152   6.090e-04 11.533  3.900e-04 18.009 ];
straprim1( 20, 1:7 ) = [  160  160  160   6.900e-04 11.872  4.530e-04 18.084 ];
straprim1( 21, 1:7 ) = [  168  168  168   7.770e-04 12.205  5.150e-04 18.414 ];
straprim1( 22, 1:7 ) = [  176  176  176   8.790e-04 12.404  5.850e-04 18.639 ];
straprim1( 23, 1:7 ) = [  184  184  184   9.790e-04 12.726  6.590e-04 18.906 ];
straprim1( 24, 1:7 ) = [  192  192  192   1.101e-03 12.857  7.440e-04 19.027 ];
straprim1( 25, 1:7 ) = [  200  200  200   1.213e-03 13.190  8.340e-04 19.185 ];
straprim1( 26, 1:7 ) = [  208  208  208   1.342e-03 13.411  9.330e-04 19.290 ];
straprim1( 27, 1:7 ) = [  216  216  216   1.477e-03 13.646  1.035e-03 19.474 ];
straprim1( 28, 1:7 ) = [  224  224  224   1.627e-03 13.816  1.146e-03 19.615 ];
straprim1( 29, 1:7 ) = [  232  232  232   1.779e-03 14.038  1.269e-03 19.680 ];
straprim1( 30, 1:7 ) = [  240  240  240   1.949e-03 14.186  1.394e-03 19.834 ];
straprim1( 31, 1:7 ) = [  248  248  248   2.122e-03 14.376  1.535e-03 19.874 ];
straprim1( 32, 1:7 ) = [  256  256  256   2.313e-03 14.507  1.713e-03 19.588 ];
straprim1( 33, 1:7 ) = [  264  264  264   2.580e-03 14.263  1.927e-03 19.097 ];
straprim1( 34, 1:7 ) = [  272  272  272   2.785e-03 14.451  2.089e-03 19.266 ];
straprim1( 35, 1:7 ) = [  280  280  280   3.002e-03 14.625  2.268e-03 19.358 ];
straprim1( 36, 1:7 ) = [  288  288  288   3.334e-03 14.330  2.451e-03 19.492 ];
straprim1( 37, 1:7 ) = [  296  296  296   3.692e-03 14.049  2.657e-03 19.522 ];
straprim1( 38, 1:7 ) = [  304  304  304   3.964e-03 14.175  2.867e-03 19.599 ];
straprim1( 39, 1:7 ) = [  312  312  312   4.247e-03 14.302  3.082e-03 19.709 ];
straprim1( 40, 1:7 ) = [  320  320  320   4.533e-03 14.458  3.317e-03 19.758 ];
straprim1( 41, 1:7 ) = [  328  328  328   4.848e-03 14.558  3.550e-03 19.880 ];
straprim1( 42, 1:7 ) = [  336  336  336   5.178e-03 14.652  3.803e-03 19.949 ];
straprim1( 43, 1:7 ) = [  344  344  344   5.516e-03 14.760  4.065e-03 20.028 ];
straprim1( 44, 1:7 ) = [  352  352  352   5.817e-03 14.995  4.339e-03 20.103 ];
straprim1( 45, 1:7 ) = [  360  360  360   6.217e-03 15.009  4.628e-03 20.162 ];
straprim1( 46, 1:7 ) = [  368  368  368   6.492e-03 15.353  4.924e-03 20.242 ];
straprim1( 47, 1:7 ) = [  376  376  376   6.880e-03 15.453  5.233e-03 20.316 ];
straprim1( 48, 1:7 ) = [  384  384  384   7.182e-03 15.768  5.586e-03 20.273 ];
straprim1( 49, 1:7 ) = [  392  392  392   7.492e-03 16.080  5.927e-03 20.326 ];
straprim1( 50, 1:7 ) = [  400  400  400   7.883e-03 16.237  6.281e-03 20.379 ];
straprim1( 51, 1:7 ) = [  408  408  408   8.368e-03 16.233  6.641e-03 20.454 ];
straprim1( 52, 1:7 ) = [  416  416  416   8.835e-03 16.297  7.025e-03 20.496 ];
straprim1( 53, 1:7 ) = [  424  424  424   9.352e-03 16.301  7.459e-03 20.438 ];
straprim1( 54, 1:7 ) = [  432  432  432   9.887e-03 16.309  7.881e-03 20.460 ];
straprim1( 55, 1:7 ) = [  440  440  440   1.054e-02 16.159  8.363e-03 20.372 ];
straprim1( 56, 1:7 ) = [  448  448  448   1.128e-02 15.944  8.816e-03 20.398 ];
straprim1( 57, 1:7 ) = [  456  456  456   1.189e-02 15.944  9.305e-03 20.380 ];
straprim1( 58, 1:7 ) = [  464  464  464   1.249e-02 15.991  9.895e-03 20.191 ];
straprim1( 59, 1:7 ) = [  472  472  472   1.314e-02 15.999  1.036e-02 20.298 ];
straprim1( 60, 1:7 ) = [  480  480  480   1.374e-02 16.092  1.087e-02 20.339 ];
straprim1( 61, 1:7 ) = [  488  488  488   1.438e-02 16.161  1.147e-02 20.257 ];
straprim1( 62, 1:7 ) = [  496  496  496   1.507e-02 16.190  1.206e-02 20.243 ];
straprim1( 63, 1:7 ) = [  504  504  504   1.578e-02 16.225  1.269e-02 20.174 ];
straprim1( 64, 1:7 ) = [  512  512  512   1.659e-02 16.184  1.344e-02 19.968 ];
straprim1( 65, 1:7 ) = [  520  520  520   1.754e-02 16.029  1.414e-02 19.882 ];
straprim1( 66, 1:7 ) = [  528  528  528   1.825e-02 16.130  1.477e-02 19.936 ];
straprim1( 67, 1:7 ) = [  536  536  536   1.900e-02 16.206  1.542e-02 19.972 ];
straprim1( 68, 1:7 ) = [  544  544  544   1.981e-02 16.250  1.613e-02 19.963 ];
straprim1( 69, 1:7 ) = [  552  552  552   2.062e-02 16.311  1.682e-02 20.000 ];
straprim1( 70, 1:7 ) = [  560  560  560   2.141e-02 16.408  1.757e-02 19.986 ];
straprim1( 71, 1:7 ) = [  568  568  568   2.225e-02 16.475  1.829e-02 20.035 ];
straprim1( 72, 1:7 ) = [  576  576  576   2.313e-02 16.521  1.906e-02 20.052 ];
straprim1( 73, 1:7 ) = [  584  584  584   2.404e-02 16.570  1.986e-02 20.060 ];
straprim1( 74, 1:7 ) = [  592  592  592   2.483e-02 16.709  2.068e-02 20.068 ];
straprim1( 75, 1:7 ) = [  600  600  600   2.576e-02 16.772  2.149e-02 20.105 ];
straprim1( 76, 1:7 ) = [  608  608  608   2.674e-02 16.811  2.233e-02 20.131 ];
straprim1( 77, 1:7 ) = [  616  616  616   2.768e-02 16.889  2.317e-02 20.174 ];
straprim1( 78, 1:7 ) = [  624  624  624   2.868e-02 16.945  2.405e-02 20.201 ];
straprim1( 79, 1:7 ) = [  632  632  632   2.969e-02 17.002  2.493e-02 20.249 ];
straprim1( 80, 1:7 ) = [  640  640  640   3.075e-02 17.053  2.587e-02 20.264 ];
straprim1( 81, 1:7 ) = [  648  648  648   3.178e-02 17.123  2.682e-02 20.291 ];
straprim1( 82, 1:7 ) = [  656  656  656   3.291e-02 17.154  2.804e-02 20.133 ];
straprim1( 83, 1:7 ) = [  664  664  664   3.400e-02 17.223  2.880e-02 20.332 ];
straprim1( 84, 1:7 ) = [  672  672  672   3.511e-02 17.286  2.970e-02 20.436 ];
straprim1( 85, 1:7 ) = [  680  680  680   3.631e-02 17.318  3.076e-02 20.442 ];
straprim1( 86, 1:7 ) = [  688  688  688   3.753e-02 17.354  3.185e-02 20.451 ];
straprim1( 87, 1:7 ) = [  696  696  696   3.867e-02 17.439  3.296e-02 20.459 ];
straprim1( 88, 1:7 ) = [  704  704  704   3.993e-02 17.476  3.409e-02 20.468 ];
straprim1( 89, 1:7 ) = [  712  712  712   4.122e-02 17.514  3.515e-02 20.539 ];
straprim1( 90, 1:7 ) = [  720  720  720   4.249e-02 17.568  3.634e-02 20.542 ];
straprim1( 91, 1:7 ) = [  728  728  728   4.382e-02 17.611  3.749e-02 20.585 ];
straprim1( 92, 1:7 ) = [  736  736  736   4.511e-02 17.675  3.867e-02 20.621 ];
straprim1( 93, 1:7 ) = [  744  744  744   4.649e-02 17.718  3.991e-02 20.640 ];
straprim1( 94, 1:7 ) = [  752  752  752   4.791e-02 17.754  4.112e-02 20.684 ];
straprim1( 95, 1:7 ) = [  760  760  760   4.931e-02 17.804  4.245e-02 20.683 ];
straprim1( 96, 1:7 ) = [  768  768  768   5.093e-02 17.789  4.418e-02 20.506 ];
straprim1( 97, 1:7 ) = [  776  776  776   5.302e-02 17.626  4.613e-02 20.261 ];
straprim1( 98, 1:7 ) = [  784  784  784   5.446e-02 17.696  4.733e-02 20.362 ];
straprim1( 99, 1:7 ) = [  792  792  792   5.601e-02 17.738  4.874e-02 20.385 ];
straprim1( 100, 1:7 ) = [  800  800  800   5.766e-02 17.760  5.028e-02 20.366 ];
straprim1( 101, 1:7 ) = [  808  808  808   5.907e-02 17.862  5.159e-02 20.449 ];
straprim1( 102, 1:7 ) = [  816  816  816   6.074e-02 17.892  5.305e-02 20.483 ];
straprim1( 103, 1:7 ) = [  824  824  824   6.237e-02 17.940  5.461e-02 20.491 ];
straprim1( 104, 1:7 ) = [  832  832  832   6.402e-02 17.992  5.612e-02 20.525 ];
straprim1( 105, 1:7 ) = [  840  840  840   6.575e-02 18.030  5.767e-02 20.556 ];
straprim1( 106, 1:7 ) = [  848  848  848   6.746e-02 18.078  5.922e-02 20.593 ];
straprim1( 107, 1:7 ) = [  856  856  856   6.933e-02 18.093  6.091e-02 20.596 ];
straprim1( 108, 1:7 ) = [  864  864  864   7.102e-02 18.164  6.247e-02 20.649 ];
straprim1( 109, 1:7 ) = [  872  872  872   7.300e-02 18.166  6.428e-02 20.630 ];
straprim1( 110, 1:7 ) = [  880  880  880   7.479e-02 18.225  6.594e-02 20.669 ];
straprim1( 111, 1:7 ) = [  888  888  888   7.670e-02 18.258  6.796e-02 20.607 ];
straprim1( 112, 1:7 ) = [  896  896  896   7.861e-02 18.302  6.952e-02 20.694 ];
straprim1( 113, 1:7 ) = [  904  904  904   8.056e-02 18.341  7.129e-02 20.724 ];
straprim1( 114, 1:7 ) = [  912  912  912   8.255e-02 18.379  7.304e-02 20.771 ];
straprim1( 115, 1:7 ) = [  920  920  920   8.471e-02 18.384  7.484e-02 20.808 ];
straprim1( 116, 1:7 ) = [  928  928  928   8.659e-02 18.458  7.667e-02 20.847 ];
straprim1( 117, 1:7 ) = [  936  936  936   8.868e-02 18.494  7.873e-02 20.832 ];
straprim1( 118, 1:7 ) = [  944  944  944   9.084e-02 18.521  8.047e-02 20.908 ];
straprim1( 119, 1:7 ) = [  952  952  952   9.299e-02 18.557  8.268e-02 20.871 ];
straprim1( 120, 1:7 ) = [  960  960  960   9.524e-02 18.580  8.453e-02 20.932 ];
straprim1( 121, 1:7 ) = [  968  968  968   9.743e-02 18.619  8.663e-02 20.942 ];
straprim1( 122, 1:7 ) = [  976  976  976   9.967e-02 18.656  8.886e-02 20.926 ];
straprim1( 123, 1:7 ) = [  984  984  984   1.020e-01 18.682  9.101e-02 20.938 ];
straprim1( 124, 1:7 ) = [  992  992  992   1.043e-01 18.716  9.313e-02 20.965 ];
straprim1( 125, 1:7 ) = [ 1000 1000 1000   1.067e-01 18.744  9.511e-02 21.029 ];
straprim1( 126, 1:7 ) = [ 1008 1008 1008   1.092e-01 18.756  9.743e-02 21.025 ];
straprim1( 127, 1:7 ) = [ 1016 1016 1016   1.117e-01 18.783  9.987e-02 21.002 ];
straprim1( 128, 1:7 ) = [ 1024 1024 1024   1.152e-01 18.639  1.044e-01 20.567 ];
straprim1( 129, 1:7 ) = [ 1032 1032 1032   1.179e-01 18.651  1.062e-01 20.697 ];
straprim1( 130, 1:7 ) = [ 1040 1040 1040   1.205e-01 18.668  1.084e-01 20.755 ];
straprim1( 131, 1:7 ) = [ 1048 1048 1048   1.226e-01 18.772  1.105e-01 20.827 ];
straprim1( 132, 1:7 ) = [ 1056 1056 1056   1.250e-01 18.836  1.133e-01 20.795 ];
straprim1( 133, 1:7 ) = [ 1064 1064 1064   1.277e-01 18.869  1.157e-01 20.822 ];
straprim1( 134, 1:7 ) = [ 1072 1072 1072   1.304e-01 18.893  1.183e-01 20.835 ];
straprim1( 135, 1:7 ) = [ 1080 1080 1080   1.332e-01 18.921  1.208e-01 20.850 ];
straprim1( 136, 1:7 ) = [ 1088 1088 1088   1.361e-01 18.932  1.235e-01 20.861 ];
straprim1( 137, 1:7 ) = [ 1096 1096 1096   1.389e-01 18.953  1.260e-01 20.895 ];
straprim1( 138, 1:7 ) = [ 1104 1104 1104   1.417e-01 18.996  1.287e-01 20.915 ];
straprim1( 139, 1:7 ) = [ 1112 1112 1112   1.446e-01 19.015  1.313e-01 20.948 ];
straprim1( 140, 1:7 ) = [ 1120 1120 1120   1.475e-01 19.048  1.339e-01 20.987 ];
straprim1( 141, 1:7 ) = [ 1128 1128 1128   1.504e-01 19.081  1.369e-01 20.975 ];
straprim1( 142, 1:7 ) = [ 1136 1136 1136   1.534e-01 19.108  1.395e-01 21.013 ];
straprim1( 143, 1:7 ) = [ 1144 1144 1144   1.566e-01 19.126  1.423e-01 21.040 ];
straprim1( 144, 1:7 ) = [ 1152 1152 1152   1.595e-01 19.169  1.453e-01 21.047 ];
straprim1( 145, 1:7 ) = [ 1160 1160 1160   1.628e-01 19.178  1.485e-01 21.017 ];
straprim1( 146, 1:7 ) = [ 1168 1168 1168   1.660e-01 19.203  1.514e-01 21.049 ];
straprim1( 147, 1:7 ) = [ 1176 1176 1176   1.694e-01 19.205  1.539e-01 21.131 ];
straprim1( 148, 1:7 ) = [ 1184 1184 1184   1.724e-01 19.255  1.572e-01 21.121 ];
straprim1( 149, 1:7 ) = [ 1192 1192 1192   1.765e-01 19.193  1.605e-01 21.102 ];
straprim1( 150, 1:7 ) = [ 1200 1200 1200   1.790e-01 19.308  1.633e-01 21.165 ];
straprim1( 151, 1:7 ) = [ 1208 1208 1208   1.831e-01 19.255  1.662e-01 21.209 ];
straprim1( 152, 1:7 ) = [ 1216 1216 1216   1.860e-01 19.334  1.700e-01 21.155 ];
straprim1( 153, 1:7 ) = [ 1224 1224 1224   1.902e-01 19.285  1.727e-01 21.241 ];
straprim1( 154, 1:7 ) = [ 1232 1232 1232   1.926e-01 19.419  1.760e-01 21.245 ];
straprim1( 155, 1:7 ) = [ 1240 1240 1240   1.970e-01 19.352  1.794e-01 21.259 ];
straprim1( 156, 1:7 ) = [ 1248 1248 1248   1.996e-01 19.473  1.831e-01 21.233 ];
straprim1( 157, 1:7 ) = [ 1256 1256 1256   2.044e-01 19.384  1.863e-01 21.268 ];
straprim1( 158, 1:7 ) = [ 1264 1264 1264   2.082e-01 19.401  1.899e-01 21.269 ];
straprim1( 159, 1:7 ) = [ 1272 1272 1272   2.123e-01 19.388  1.938e-01 21.237 ];
straprim1( 160, 1:7 ) = [ 1280 1280 1280   2.162e-01 19.396  1.990e-01 21.077 ];
straprim1( 161, 1:7 ) = [ 1288 1288 1288   2.217e-01 19.273  2.041e-01 20.940 ];
straprim1( 162, 1:7 ) = [ 1296 1296 1296   2.253e-01 19.324  2.069e-01 21.037 ];
straprim1( 163, 1:7 ) = [ 1304 1304 1304   2.294e-01 19.330  2.106e-01 21.062 ];
straprim1( 164, 1:7 ) = [ 1312 1312 1312   2.336e-01 19.333  2.144e-01 21.066 ];
straprim1( 165, 1:7 ) = [ 1320 1320 1320   2.374e-01 19.380  2.180e-01 21.103 ];
straprim1( 166, 1:7 ) = [ 1328 1328 1328   2.416e-01 19.386  2.219e-01 21.107 ];
straprim1( 167, 1:7 ) = [ 1336 1336 1336   2.456e-01 19.420  2.260e-01 21.101 ];
straprim1( 168, 1:7 ) = [ 1344 1344 1344   2.495e-01 19.457  2.297e-01 21.134 ];
straprim1( 169, 1:7 ) = [ 1352 1352 1352   2.544e-01 19.432  2.338e-01 21.141 ];
straprim1( 170, 1:7 ) = [ 1360 1360 1360   2.584e-01 19.468  2.374e-01 21.189 ];
straprim1( 171, 1:7 ) = [ 1368 1368 1368   2.628e-01 19.484  2.417e-01 21.183 ];
straprim1( 172, 1:7 ) = [ 1376 1376 1376   2.669e-01 19.525  2.456e-01 21.214 ];
straprim1( 173, 1:7 ) = [ 1384 1384 1384   2.714e-01 19.533  2.497e-01 21.234 ];
straprim1( 174, 1:7 ) = [ 1392 1392 1392   2.756e-01 19.571  2.542e-01 21.222 ];
straprim1( 175, 1:7 ) = [ 1400 1400 1400   2.805e-01 19.566  2.579e-01 21.280 ];
straprim1( 176, 1:7 ) = [ 1408 1408 1408   2.848e-01 19.601  2.626e-01 21.259 ];
straprim1( 177, 1:7 ) = [ 1416 1416 1416   2.897e-01 19.604  2.668e-01 21.283 ];
straprim1( 178, 1:7 ) = [ 1424 1424 1424   2.945e-01 19.610  2.715e-01 21.272 ];
straprim1( 179, 1:7 ) = [ 1432 1432 1432   2.989e-01 19.650  2.757e-01 21.304 ];
straprim1( 180, 1:7 ) = [ 1440 1440 1440   3.034e-01 19.684  2.803e-01 21.305 ];
straprim1( 181, 1:7 ) = [ 1448 1448 1448   3.086e-01 19.678  2.847e-01 21.328 ];
straprim1( 182, 1:7 ) = [ 1456 1456 1456   3.132e-01 19.711  2.898e-01 21.300 ];
straprim1( 183, 1:7 ) = [ 1464 1464 1464   3.182e-01 19.722  2.939e-01 21.356 ];
straprim1( 184, 1:7 ) = [ 1472 1472 1472   3.229e-01 19.755  2.991e-01 21.326 ];
straprim1( 185, 1:7 ) = [ 1480 1480 1480   3.284e-01 19.746  3.032e-01 21.382 ];
straprim1( 186, 1:7 ) = [ 1488 1488 1488   3.332e-01 19.776  3.082e-01 21.381 ];
straprim1( 187, 1:7 ) = [ 1496 1496 1496   3.382e-01 19.800  3.125e-01 21.429 ];
straprim1( 188, 1:7 ) = [ 1504 1504 1504   3.429e-01 19.843  3.175e-01 21.427 ];
straprim1( 189, 1:7 ) = [ 1512 1512 1512   3.485e-01 19.837  3.224e-01 21.443 ];
straprim1( 190, 1:7 ) = [ 1520 1520 1520   3.540e-01 19.843  3.279e-01 21.421 ];
straprim1( 191, 1:7 ) = [ 1528 1528 1528   3.595e-01 19.846  3.339e-01 21.367 ];
straprim1( 192, 1:7 ) = [ 1536 1536 1536   3.663e-01 19.786  3.454e-01 20.985 ];
straprim1( 193, 1:7 ) = [ 1544 1544 1544   3.738e-01 19.694  3.495e-01 21.064 ];
straprim1( 194, 1:7 ) = [ 1552 1552 1552   3.754e-01 19.914  3.528e-01 21.194 ];
straprim1( 195, 1:7 ) = [ 1560 1560 1560   3.834e-01 19.804  3.578e-01 21.220 ];
straprim1( 196, 1:7 ) = [ 1568 1568 1568   3.865e-01 19.949  3.644e-01 21.161 ];
straprim1( 197, 1:7 ) = [ 1576 1576 1576   3.948e-01 19.829  3.679e-01 21.279 ];
straprim1( 198, 1:7 ) = [ 1584 1584 1584   3.980e-01 19.972  3.734e-01 21.287 ];
straprim1( 199, 1:7 ) = [ 1592 1592 1592   4.067e-01 19.840  3.785e-01 21.320 ];
straprim1( 200, 1:7 ) = [ 1600 1600 1600   4.095e-01 20.005  3.851e-01 21.275 ];
straprim1( 201, 1:7 ) = [ 1608 1608 1608   4.185e-01 19.872  3.898e-01 21.333 ];
straprim1( 202, 1:7 ) = [ 1616 1616 1616   4.210e-01 20.048  3.954e-01 21.346 ];
straprim1( 203, 1:7 ) = [ 1624 1624 1624   4.303e-01 19.906  4.006e-01 21.382 ];
straprim1( 204, 1:7 ) = [ 1632 1632 1632   4.329e-01 20.083  4.064e-01 21.392 ];
straprim1( 205, 1:7 ) = [ 1640 1640 1640   4.420e-01 19.961  4.127e-01 21.376 ];
straprim1( 206, 1:7 ) = [ 1648 1648 1648   4.489e-01 19.943  4.181e-01 21.409 ];
straprim1( 207, 1:7 ) = [ 1656 1656 1656   4.550e-01 19.960  4.242e-01 21.412 ];
straprim1( 208, 1:7 ) = [ 1664 1664 1664   4.609e-01 19.994  4.303e-01 21.416 ];
straprim1( 209, 1:7 ) = [ 1672 1672 1672   4.671e-01 20.012  4.364e-01 21.420 ];
straprim1( 210, 1:7 ) = [ 1680 1680 1680   4.740e-01 20.007  4.430e-01 21.405 ];
straprim1( 211, 1:7 ) = [ 1688 1688 1688   4.803e-01 20.027  4.478e-01 21.482 ];
straprim1( 212, 1:7 ) = [ 1696 1696 1696   4.865e-01 20.055  4.542e-01 21.484 ];
straprim1( 213, 1:7 ) = [ 1704 1704 1704   4.934e-01 20.058  4.609e-01 21.469 ];
straprim1( 214, 1:7 ) = [ 1712 1712 1712   4.996e-01 20.088  4.669e-01 21.493 ];
straprim1( 215, 1:7 ) = [ 1720 1720 1720   5.065e-01 20.094  4.731e-01 21.513 ];
straprim1( 216, 1:7 ) = [ 1728 1728 1728   5.135e-01 20.095  4.799e-01 21.502 ];
straprim1( 217, 1:7 ) = [ 1736 1736 1736   5.205e-01 20.104  4.863e-01 21.515 ];
straprim1( 218, 1:7 ) = [ 1744 1744 1744   5.227e-01 20.295  4.931e-01 21.515 ];
straprim1( 219, 1:7 ) = [ 1752 1752 1752   5.341e-01 20.136  4.993e-01 21.541 ];
straprim1( 220, 1:7 ) = [ 1760 1760 1760   5.369e-01 20.310  5.064e-01 21.533 ];
straprim1( 221, 1:7 ) = [ 1768 1768 1768   5.482e-01 20.163  5.132e-01 21.536 ];
straprim1( 222, 1:7 ) = [ 1776 1776 1776   5.509e-01 20.336  5.201e-01 21.541 ];
straprim1( 223, 1:7 ) = [ 1784 1784 1784   5.629e-01 20.175  5.271e-01 21.545 ];
straprim1( 224, 1:7 ) = [ 1792 1792 1792   5.659e-01 20.339  5.392e-01 21.344 ];
straprim1( 225, 1:7 ) = [ 1800 1800 1800   5.805e-01 20.094  5.469e-01 21.327 ];
straprim1( 226, 1:7 ) = [ 1808 1808 1808   5.881e-01 20.097  5.528e-01 21.382 ];
straprim1( 227, 1:7 ) = [ 1816 1816 1816   5.955e-01 20.115  5.592e-01 21.420 ];
straprim1( 228, 1:7 ) = [ 1824 1824 1824   6.030e-01 20.127  5.667e-01 21.418 ];
straprim1( 229, 1:7 ) = [ 1832 1832 1832   6.106e-01 20.139  5.746e-01 21.403 ];
straprim1( 230, 1:7 ) = [ 1840 1840 1840   6.182e-01 20.153  5.822e-01 21.401 ];
straprim1( 231, 1:7 ) = [ 1848 1848 1848   6.259e-01 20.166  5.889e-01 21.432 ];
straprim1( 232, 1:7 ) = [ 1856 1856 1856   6.332e-01 20.195  5.971e-01 21.416 ];
straprim1( 233, 1:7 ) = [ 1864 1864 1864   6.412e-01 20.202  6.034e-01 21.467 ];
straprim1( 234, 1:7 ) = [ 1872 1872 1872   6.493e-01 20.208  6.122e-01 21.433 ];
straprim1( 235, 1:7 ) = [ 1880 1880 1880   6.572e-01 20.221  6.182e-01 21.495 ];
straprim1( 236, 1:7 ) = [ 1888 1888 1888   6.641e-01 20.268  6.257e-01 21.511 ];
straprim1( 237, 1:7 ) = [ 1896 1896 1896   6.728e-01 20.262  6.334e-01 21.520 ];
straprim1( 238, 1:7 ) = [ 1904 1904 1904   6.806e-01 20.285  6.415e-01 21.520 ];
straprim1( 239, 1:7 ) = [ 1912 1912 1912   6.890e-01 20.289  6.492e-01 21.534 ];
straprim1( 240, 1:7 ) = [ 1920 1920 1920   6.961e-01 20.336  6.574e-01 21.533 ];
straprim1( 241, 1:7 ) = [ 1928 1928 1928   7.059e-01 20.307  6.667e-01 21.500 ];
straprim1( 242, 1:7 ) = [ 1936 1936 1936   7.140e-01 20.326  6.736e-01 21.545 ];
straprim1( 243, 1:7 ) = [ 1944 1944 1944   7.228e-01 20.327  6.816e-01 21.557 ];
straprim1( 244, 1:7 ) = [ 1952 1952 1952   7.316e-01 20.333  6.899e-01 21.562 ];
straprim1( 245, 1:7 ) = [ 1960 1960 1960   7.397e-01 20.358  6.973e-01 21.596 ];
straprim1( 246, 1:7 ) = [ 1968 1968 1968   7.483e-01 20.372  7.065e-01 21.578 ];
straprim1( 247, 1:7 ) = [ 1976 1976 1976   7.574e-01 20.375  7.148e-01 21.587 ];
straprim1( 248, 1:7 ) = [ 1984 1984 1984   7.665e-01 20.378  7.241e-01 21.570 ];
straprim1( 249, 1:7 ) = [ 1992 1992 1992   7.744e-01 20.414  7.316e-01 21.607 ];
straprim1( 250, 1:7 ) = [ 2000 2000 2000   7.842e-01 20.404  7.402e-01 21.616 ];
straprim1( 251, 1:7 ) = [ 2008 2008 2008   7.927e-01 20.428  7.500e-01 21.590 ];
straprim1( 252, 1:7 ) = [ 2016 2016 2016   8.003e-01 20.476  7.577e-01 21.628 ];
straprim1( 253, 1:7 ) = [ 2024 2024 2024   8.110e-01 20.448  7.666e-01 21.631 ];
straprim1( 254, 1:7 ) = [ 2032 2032 2032   8.205e-01 20.452  7.752e-01 21.647 ];
straprim1( 255, 1:7 ) = [ 2040 2040 2040   8.317e-01 20.414  7.848e-01 21.635 ];
straprim1( 256, 1:7 ) = [ 2048 2048 2048   8.384e-01 20.492  8.228e-01 20.880 ];
