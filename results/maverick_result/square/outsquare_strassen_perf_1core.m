clear;
outsquare_1_1level_1core;
outsquare_5_2level_1core;

outsquare_9_hybrid_1core;

outsquare_13_1levelref_1core;
outsquare_17_2levelref_1core;

outsquare_21_mkl_1core;


% ---------------------------------------------------------
% Plotting
% ---------------------------------------------------------
figure;
% hFig = figure(1);
%set(hFig, 'Position', [0 0 160 240])

set( gcf, 'PaperSize', [3 3]);
set( gcf, 'PaperPosition', [0.25 0.25 3 3] );
set( gcf, 'Position', [0 0 420 400]);
%set( gcf, 'Position', [0 0 600*2 400*2]);


hold;

plot( sb_stra_1level( :, 1 ), sb_stra_1level( :, 5), '--', 'LineWidth', 1.3, 'Color',  'k');
plot( sb_mkl_gemm( :, 1 ), sb_mkl_gemm( :, 5), '.-', 'LineWidth', 1.3, 'Color',  'k');

plot( sb_stra_1level( :, 1 ), sb_stra_1level( :, 8), '.-', 'LineWidth', 1.3, 'Color',  [0 0.2 1.0] );
plot( sb_stra_2level( :, 1 ), sb_stra_2level( :, 8), '.-', 'LineWidth', 1.3, 'Color', [1 0 0.2] );

%plot( data_gemm_blis3( :, 1 ), data_gemm_blis3( :, 6), '.-', 'LineWidth', 2, 'Color',  'y' );
plot( sb_stra2_2level( :, 1 ), sb_stra2_2level( :, 8), '.-', 'LineWidth', 1.3, 'Color', 'm' );

plot( sb_stra_1levelref_par( :, 1 ), sb_stra_1levelref_par( :, 8), '--', 'LineWidth', 1.3, 'Color',  [0 0.2 1.0] );
plot( sb_stra_2levelref_par( :, 1 ), sb_stra_2levelref_par( :, 8), '--', 'LineWidth', 1.3, 'Color', [1 0 0.2] );




xlabel( 'm=k=n' );
ylabel( 'Effective GFLOPS (2\cdotm\cdotn\cdotk/time)' );
title( 'm=k=n, 1 core' );

grid on;
axis square;
axis( [ 0 12000 0 35 ] );
%axis( [ 0 5000 0 248 ] );

ax = gca;
ax.YTick = [  0, 5, 10, 15, 20, 25, 28.32, 30, 35];
%ax.YTick = [  0, 50, 100, 150, 200, 248];

%ax.XTick = [ 0, 200, 400, 600, 800, 1000];
%ax.XTick = [ 0, 1000, 2000, 3000, 4000 ];
ax.XTick = [ 0, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000, 11000, 12000];
ax.XAxis.Exponent = 3;


set( gca,'FontSize', 12 );

legend( 'BLIS DGEMM', ...
        'MKL DGEMM', ...
        'One-level Strassen', ...
        'Two-level Strassen', ...
        'Two-level Hybrid Strassen', ...
        'One-level Naive Strassen', ...
        'Two-level Naive Strassen', ...
        'Location','SouthEast');
