clear;
test_stra_1level_fixk_10core;
test_stra_2level_fixk_10core;
test_stra_hybrid_fixk_10core;
test_stra_1levelref_fixk_10core;
test_stra_2levelref_fixk_10core;
test_mkl_fixk_10core;

core_num=10;

% ---------------------------------------------------------
% Plotting
% ---------------------------------------------------------
hFig = figure;
% hFig = figure(1);
%set(hFig, 'Position', [0 0 160 240])

set( gcf, 'PaperSize', [3 3]);
set( gcf, 'PaperPosition', [0.25 0.25 3 3] );
%set( gcf, 'Position', [0 0 600 400]);
set( gcf, 'Position', 1.0 .* [0 0 420 400]);
%set( gcf, 'Position', [0 0 600*2 400*2]);


hold;

plot( sb_stra_1level( :, 1 ), sb_stra_1level( :, 5), '--', 'LineWidth', 1.3, 'Color',  'k');
plot( sb_mkl_gemm( :, 1 ), sb_mkl_gemm( :, 5), '.-', 'LineWidth', 1.3, 'Color',  'k');

plot( sb_stra_1level( :, 1 ), sb_stra_1level( :, 8), '.-', 'LineWidth', 1.3, 'Color',  [0 0.2 1.0] );
plot( sb_stra_2level( :, 1 ), sb_stra_2level( :, 8), '.-', 'LineWidth', 1.3, 'Color', [1 0 0.2] );

plot( sb_stra2_2level( :, 1 ), sb_stra2_2level( :, 8), '.-', 'LineWidth', 1.3, 'Color', 'm' );

plot( sb_stra_1levelref_par( :, 1 ), sb_stra_1levelref_par( :, 8), '--', 'LineWidth', 1.3, 'Color',  [0 0.2 1.0] );
plot( sb_stra_2levelref_par( :, 1 ), sb_stra_2levelref_par( :, 8), '--', 'LineWidth', 1.3, 'Color', [1 0 0.2] );


xlabel( 'm=n' );
ylabel( 'Effective GFLOPS (2\cdotm\cdotn\cdotk/time)' );
title( 'k=2048, m=n vary, 10 core' );

grid on;
axis square;
axis( [ 0 20000 0 24.8*core_num ] );
%axis( [ 0 5000 0 248 ] );

ax = gca;
%ax.YTick = core_num .* [  0, 5, 10, 15, 20, 24.8, 25.5, 30, 35];
ax.YTick = core_num .* [  0, 5, 10, 15, 20, 24.8, 30, 35];
%ax.YTick = [  0, 50, 100, 150, 200, 248];

%ax.XTick = [ 0, 200, 400, 600, 800, 1000];
%ax.XTick = [ 0, 1000, 2000, 3000, 4000 ];
ax.XTick = [ 0, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000, 11000, 12000, 13000, 14000, 15000, 16000, 17000, 18000, 19000, 20000];
ax.XAxis.Exponent = 3;


set( gca,'FontSize', 12 );

legend( 'BLIS DGEMM', ...
        'MKL DGEMM', ...
        'One-level Strassen', ...
        'Two-level Strassen', ...
         'Two-level Hybrid Strassen', ...
         'One-level Naive Strassen', ...
        'Two-level Naive Strassen', ...
        'Location','SouthEast');
    
 %print('FillPageFigure','-dpdf','-fillpage')
 %saveas(gcf, 'test', 'pdf')
 
%      pos = get(gca, 'Position');
%     pos(1) = 0.055;
%     pos(3) = 0.9;
%     set(gca, 'Position', pos)
 
 %set(gca(), 'LooseInset', get(gca(), 'TightInset'));
 
 %set(hFig, 'InvertHardcopy', 'off');
%  tightInset = get(gca, 'TightInset');
%  position(1) = tightInset(1)+0.1;
%  position(2) = tightInset(2);
%  position(3) = 1 - tightInset(1) - tightInset(3)-0.31;
%  position(4) = 1 - tightInset(2) - tightInset(4);
%  set(gca, 'Position', position);
 %saveas(h, 'WithoutMargins.pdf');
