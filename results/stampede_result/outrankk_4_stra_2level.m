data_gemm_blis4(  1, 1:7 ) = [  200   1.128e+00 22.694  9.265e-11  1.904e+00 13.446  9.437e-11 ];
data_gemm_blis4(  2, 1:7 ) = [  400   2.267e+00 22.589  1.394e-10  2.767e+00 18.501  1.463e-10 ];
data_gemm_blis4(  3, 1:7 ) = [  600   3.409e+00 22.526  1.786e-10  3.653e+00 21.021  1.877e-10 ];
data_gemm_blis4(  4, 1:7 ) = [  800   4.553e+00 22.492  1.953e-10  4.510e+00 22.703  2.121e-10 ];
data_gemm_blis4(  5, 1:7 ) = [ 1000   5.624e+00 22.758  2.179e-10  5.390e+00 23.748  2.417e-10 ];
data_gemm_blis4(  6, 1:7 ) = [ 1200   6.757e+00 22.731  2.358e-10  6.485e+00 23.686  2.589e-10 ];
data_gemm_blis4(  7, 1:7 ) = [ 1400   7.902e+00 22.677  2.595e-10  7.347e+00 24.391  2.808e-10 ];
data_gemm_blis4(  8, 1:7 ) = [ 1600   9.046e+00 22.639  2.726e-10  8.209e+00 24.948  2.978e-10 ];
data_gemm_blis4(  9, 1:7 ) = [ 1800   1.021e+01 22.569  2.921e-10  9.068e+00 25.408  3.190e-10 ];
data_gemm_blis4( 10, 1:7 ) = [ 2000   1.125e+01 22.751  3.137e-10  9.959e+00 25.705  3.455e-10 ];
data_gemm_blis4( 11, 1:7 ) = [ 2200   1.239e+01 22.721  3.249e-10  1.108e+01 25.413  3.622e-10 ];
data_gemm_blis4( 12, 1:7 ) = [ 2400   1.354e+01 22.693  3.402e-10  1.194e+01 25.739  3.719e-10 ];
data_gemm_blis4( 13, 1:7 ) = [ 2600   1.469e+01 22.662  3.597e-10  1.281e+01 25.973  3.914e-10 ];
data_gemm_blis4( 14, 1:7 ) = [ 2800   1.576e+01 22.745  3.819e-10  1.402e+01 25.565  4.171e-10 ];
data_gemm_blis4( 15, 1:7 ) = [ 3000   1.690e+01 22.724  3.976e-10  1.493e+01 25.727  4.344e-10 ];
data_gemm_blis4( 16, 1:7 ) = [ 3200   1.805e+01 22.696  4.088e-10  1.608e+01 25.470  4.508e-10 ];
data_gemm_blis4( 17, 1:7 ) = [ 3400   1.919e+01 22.673  4.142e-10  1.696e+01 25.661  4.543e-10 ];
data_gemm_blis4( 18, 1:7 ) = [ 3600   2.034e+01 22.653  4.211e-10  1.784e+01 25.829  4.616e-10 ];
data_gemm_blis4( 19, 1:7 ) = [ 3800   2.140e+01 22.732  4.433e-10  1.873e+01 25.974  4.867e-10 ];
data_gemm_blis4( 20, 1:7 ) = [ 4000   2.253e+01 22.726  4.543e-10  1.962e+01 26.094  4.992e-10 ];
data_gemm_blis4( 21, 1:7 ) = [ 4200   2.369e+01 22.697  4.865e-10  2.067e+01 26.015  5.257e-10 ];
data_gemm_blis4( 22, 1:7 ) = [ 4400   2.482e+01 22.688  4.941e-10  2.152e+01 26.169  5.344e-10 ];
data_gemm_blis4( 23, 1:7 ) = [ 4600   2.590e+01 22.738  4.980e-10  2.240e+01 26.283  5.465e-10 ];
data_gemm_blis4( 24, 1:7 ) = [ 4800   2.702e+01 22.742  5.137e-10  2.326e+01 26.414  5.630e-10 ];
data_gemm_blis4( 25, 1:7 ) = [ 5000   2.819e+01 22.704  5.292e-10  2.415e+01 26.499  5.785e-10 ];
data_gemm_blis4( 26, 1:7 ) = [ 5200   2.932e+01 22.704  5.539e-10  2.530e+01 26.305  6.068e-10 ];
data_gemm_blis4( 27, 1:7 ) = [ 5400   3.047e+01 22.683  5.545e-10  2.617e+01 26.417  6.002e-10 ];
data_gemm_blis4( 28, 1:7 ) = [ 5600   3.153e+01 22.734  5.597e-10  2.703e+01 26.521  6.089e-10 ];
data_gemm_blis4( 29, 1:7 ) = [ 5800   3.267e+01 22.723  5.817e-10  2.790e+01 26.608  6.317e-10 ];
data_gemm_blis4( 30, 1:7 ) = [ 6000   3.381e+01 22.717  6.016e-10  2.965e+01 25.901  6.513e-10 ];
data_gemm_blis4( 31, 1:7 ) = [ 6200   3.496e+01 22.702  6.111e-10  3.082e+01 25.753  6.706e-10 ];
data_gemm_blis4( 32, 1:7 ) = [ 6400   3.603e+01 22.734  6.309e-10  3.167e+01 25.870  6.797e-10 ];
data_gemm_blis4( 33, 1:7 ) = [ 6600   3.716e+01 22.732  6.307e-10  3.256e+01 25.942  6.903e-10 ];
data_gemm_blis4( 34, 1:7 ) = [ 6800   3.831e+01 22.720  6.529e-10  3.344e+01 26.026  7.106e-10 ];
data_gemm_blis4( 35, 1:7 ) = [ 7000   3.944e+01 22.717  6.582e-10  3.435e+01 26.084  7.108e-10 ];
data_gemm_blis4( 36, 1:7 ) = [ 7200   4.060e+01 22.702  6.623e-10  3.551e+01 25.954  7.226e-10 ];
data_gemm_blis4( 37, 1:7 ) = [ 7400   4.167e+01 22.731  6.632e-10  3.637e+01 26.044  7.183e-10 ];
data_gemm_blis4( 38, 1:7 ) = [ 7600   4.279e+01 22.732  6.858e-10  3.725e+01 26.119  7.521e-10 ];
data_gemm_blis4( 39, 1:7 ) = [ 7800   4.394e+01 22.724  7.152e-10  3.815e+01 26.173  7.637e-10 ];
data_gemm_blis4( 40, 1:7 ) = [ 8000   4.508e+01 22.715  7.199e-10  3.903e+01 26.239  7.807e-10 ];
