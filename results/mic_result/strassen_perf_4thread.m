%Single GFLOP/sec =16 (SP SIMD Lane) x 2 (FMA) x 1.1 (GHZ) x 60 (# cores) = 2112 for single precision arithmetic
%Double GFLOP/sec = 8 (DP SIMD Lane) x 2 (FMA) x 1.1 (GHZ) x 60 (# cores) = 1056 for double precision arithmetic


%outfixk_1_naive_single;
%outfixk_2_naive_multi;

%output_1_gemm_1thread;
%output_2_straprim_1thread;

output_3_gemm_4thread;
output_4_straprim_4thread;


%outfixk_7_mul2stra_1level;


% ---------------------------------------------------------
% Plotting
% ---------------------------------------------------------
figure;
% hFig = figure(1);
%set(hFig, 'Position', [0 0 160 240])

set( gcf, 'PaperSize', [3 3]);
set( gcf, 'PaperPosition', [0.25 0.25 3 3] );
%set( gcf, 'Position', [0 0 600 400]);
set( gcf, 'Position', [0 0 600*2 400*2]);


hold on;

plot( mic_gemm3( :, 1 ), mic_gemm3( :, 5), '.-', 'LineWidth', 1, 'Color',  'c');

plot( mic_straprim4( :, 1 ), mic_straprim4( :, 5), '.-', 'LineWidth', 1, 'Color',  [0 0.2 1.0] );
plot( mic_straprim4( :, 1 ), mic_straprim4( :, 7), '.-', 'LineWidth', 1, 'Color', [1 0 0.2] );


%plot( data_gemm_blis3( :, 1 ), data_gemm_blis3( :, 6), '.-', 'LineWidth', 2, 'Color',  'm' );

%plot( data_gemm_blis4( :, 1 ), data_gemm_blis4( :, 6), '.-', 'LineWidth', 2, 'Color',  'g' );

%plot( data_gemm_blis5( :, 1 ), data_gemm_blis5( :, 6), '.-', 'LineWidth', 2, 'Color',  'k' );

%plot( data_gemm_blis6( :, 1 ), data_gemm_blis6( :, 6), '.-', 'LineWidth', 2, 'Color',  [1,0.4,0.6] );

%plot( data_gemm_blis7( :, 1 ), data_gemm_blis7( :, 6), '--', 'LineWidth', 2, 'Color',  'k' );


xlabel( 'm=n' );
ylabel( 'Practical GFLOPS (2n^3/time)' );
title( 'Strassen(k=240, m=n vary)' );

grid on;
axis square;
%axis( [ 0 8000 0 30 ] );
%axis( [ 0 5000 0 248 ] );
axis( [ 0 2000 0 20 ] );

ax = gca;
ax.YTick = [  0, 5, 10, 15, 20 ];
%ax.YTick = [  0, 50, 100, 150, 200, 248];

%ax.XTick = [ 0, 200, 400, 600, 800, 1000];
%ax.XTick = [ 0, 1000, 2000, 3000, 4000 ];
%ax.XTick = [ 0, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000, 11000, 12000, 13000, 14000, 15000, 16000, 17000, 18000, 19000, 20000];
ax.XTick = [ 0, 200, 400, 600, 800, 1000, 1200, 1400, 1600, 1800, 2000 ];
%ax.XAxis.Exponent = 3;


set( gca,'FontSize',16 );

legend( 'BLIS DGEMM', ...
        'BLIS straprim (ref)', ...
        'BLIS straprim (my)', ...
        'Location','SouthEast');
