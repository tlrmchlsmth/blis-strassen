from sbatch_stra_maverick import run_job
import subprocess
import os
import shutil

orig_directory = os.getcwd()

#scratch_dir = os.getenv('SCRATCH')
#work_dir = os.getenv('WORK')
##home_dir = os.getenv('HOME')

# Data parameters
my_exe_dir = '/home/03223/jianyu/Work/Github/blis-strassen/straTest/my_result/'

if not os.path.exists(my_exe_dir):
    os.makedirs(my_exe_dir)

#print my_exe_dir

# change to the working my_exe_dir
os.chdir(my_exe_dir)

### going to put a copy of the executable in the run my_exe_dir to use with core files
cur_exe_dir = '/home/03223/jianyu/Work/Github/blis-strassen/straTest/'
#shutil.copy2(cur_exe_dir + my_test_routine, my_exe_dir)

#1. core number: 1, 2, 4, 8, 16
#2. matrix type: rankk (m=n=8192, k vary from 1024..1024..8192 ), fixk (k=1024, m=n from 1024..1024..20480), square(m=n=k, vary from [256..256..20480] )
#3. range: [256, 256, 20480], 
#4. routine: stra (1level), mulstra(2level)
#ibrun tacc_affinity ./run_stra_square_4core_mnk_256_256_20480.sh
#ibrun tacc_affinity ./run_mulstra_rankk_2core_mn_8192_k_256_256_8192.sh
arg_list = ['nodes', 'procspernode', 'matrix_type', 'test_routine', 'test_label', 'core_num',
            'jobname', 'my_exe_dir', 'cur_exe_dir']

num_nodes=1
procspernode=1

#matrix_type_list = ['square', 'rankk', 'fixk'] # determine the test_range
#matrix_type_list = ['rankk', 'fixk'] # determine the test_range
matrix_type_list = ['fixk'] # determine the test_range
#matrix_type_list = ['square'] # determine the test_range
#matrix_type_list = ['rankk'] # determine the test_range
#test_routine_list = ['test_stra_1level_blis.x', 'test_stra_2level_blis.x', 'test_stra_hybrid_blis.x']
#test_label_list   = ['stra_1level', 'stra_2level', 'stra2_2level']
#test_routine_list = ['test_stra_1level_blis.x']
#test_label_list   = ['stra_1level']
#test_routine_list = [ 'test_stra_2level_blis.x' ]
#test_label_list   = [ 'stra_2level' ]
test_routine_list = [ 'test_mkl_blis.x' ]
test_label_list   = [ 'mkl_gemm' ]
#test_routine_list = [ 'test_stra_1levelref_mkl_blis.x' ]
#test_label_list   = [ 'stra_1levelref_par' ]
#test_routine_list = [ 'test_stra_hybrid_blis.x' ]
#test_label_list   = [ 'stra2_2level' ]
#test_routine_list = [ 'test_stra_1levelref_blis.x', 'test_stra_2levelref_blis.x' ]
#test_label_list   = [ 'stra_1levelref', 'stra_2levelref' ]
#test_routine_list = [ 'test_stra_2levelref_blis.x' ]
#test_label_list   = [ 'stra_2levelref' ]
#test_routine_list = ['test_stra_1level_blis.x', 'test_stra_2level_blis.x', 'test_stra_hybrid_blis.x', 'test_stra_1levelref_mkl_blis.x', 'test_stra_2levelref_mkl_blis.x' ]
#test_label_list   = ['stra_1level', 'stra_2level', 'stra2_2level', 'stra_1levelref_par', 'stra_2levelref_par' ]
#test_routine_list = [ 'test_stra_hybrid_blis.x', 'test_stra_2levelref_mkl_blis.x' ]
#test_label_list   = [ 'stra2_2level', 'stra_2levelref_par' ]
#test_range = [ [256,256,20480] ]

core_num_list=[1,5,10,20]
#core_num_list=[5,10,20]
#core_num_list=[1,5]
#core_num_list=[20]
#core_num_list=[1]

i = 0

# loop over parameters
for core_num in core_num_list:

    for matrix_type in matrix_type_list:

        routine_id = 0
        for test_routine in test_routine_list:
            test_label = test_label_list[routine_id]
            routine_id += 1

            jobname_suffix_string = ""
            #if matrix_type == 'square':
            #    jobname_suffix_string = 'mnk_256_256_20480'
            #elif matrix_type == 'rankk':
            #    jobname_suffix_string = 'mn_8000_k_200_200_8000'
            #else: #matrix_type == 'fixk':
            #    jobname_suffix_string = 'k2000_mn_2000_400_20000'

            #jobname = str(test_routine)[:-2] + '_' + str(matrix_type) + '_' + str(jobname_suffix_string) + '_' + str(core_num) + 'core';
            jobname = str(test_routine)[:-7] + '_' + str(matrix_type) + '_' + str(core_num) + 'core';

            inputs = [num_nodes, procspernode, matrix_type, test_routine, test_label, core_num,
                      jobname, my_exe_dir, cur_exe_dir]

            #print len(arg_list)
            #print len(inputs)
            
            input_dict = dict(zip(arg_list, inputs))
            
            tacc_script_filename = run_job(input_dict)

            i = i + 1
    
            res = subprocess.Popen('/usr/bin/sbatch ' + tacc_script_filename, shell=True)
            print "res:" + str(res)

# return to the original directory
os.chdir(orig_directory)


