#!/bin/bash

export MIC_KMP_AFFINITY=compact,verbose
#export MIC_KMP_AFFINITY=compact
export MIC_BLIS_JR_NT=4
export MIC_BLIS_IC_NT=1
#export MIC_BLIS_JR_NT=16
#export MIC_BLIS_IC_NT=15


#k_start=240
#k_end=2400
#k_blocksize=240

#200, 8000, 200
#256, 1032*32, 256
#1280, 1280*32, 1280
#1920, 1920*32, 1920
#240, 240*32, 240

#14400

#k_start=8
#k_end=2048
#k_blocksize=8

k_start=32
k_end=2048
k_blocksize=32
echo "mic_gemm=["
for (( k=k_start; k<=k_end; k+=k_blocksize ))
do
    ./test_gemm_blis.x     $k 240 $k 
done
echo "];"


