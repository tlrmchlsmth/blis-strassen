#!/bin/bash

#export KMP_AFFINITY=compact,verbose
export KMP_AFFINITY=compact
export OMP_NUM_THREADS=1
export BLIS_IC_NT=1
export BLIS_JR_NT=1

##The test driver needs THREE parameters: $m, $k, $n
##./Executable $m $k $n

#echo "1-level Strassen Primitive"
#./test_straprim_blis.x     1024 1024 1024
#echo "2-level Strassen Primitive"
#./test_mulstraprim_blis.x     1024 1024 1024

echo "1-level Strassen"
./test_stra_1level_blis.x     1024 1024 1024
echo "2-level Strassen"
./test_stra_2level_blis.x     1024 1024 1024
echo "2-level Hybrid Strassen"
./test_stra_hybrid_blis.x     1024 1024 1024
echo "1-level Strassen Naive"
./test_stra_1levelref_blis.x     1024 1024 1024
echo "2-level Strassen Naive"
./test_stra_2levelref_blis.x     1024 1024 1024


